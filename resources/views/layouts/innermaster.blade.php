<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Itattyou') }}</title>

    <!-- Styles -->
    <link href="{{ URL::asset('assets/css/bootstrap_front.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ URL::asset('assets/css/theme-style.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom Theme files -->
    <!----webfonts---->
    <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
     @yield('style')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('front.innerheader')
    <div class="main-page">
        @yield('content')
    </div>    
   @include('front.footer')
     
    <!-- Scripts -->
    @yield('script')
</body>
</html>
