<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Itattyou') }}</title>

    <!-- Styles -->
    <link href="{{ URL::asset('assets/css/bootstrap_front.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ URL::asset('assets/css/theme-style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/front_assets/css/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
 <style>
        .change_pro_ban_img {
            float: left;
            padding: 30px;
            width: 100%;
        }.uplod_picj_txt {
    color: #313131;
    font: 18px "open_Sanssemibold";
    margin-bottom: 10px;
    width: 100%;
}
.gym_pro_img {
    background-color: white;
    border: 1px solid #d8d8d8;
    box-shadow: 1px 2px 3px #ededed;
    height: 40px;
    position: relative;
    width: 400px;
}
.cropit-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px;
    height: 200px;
    margin: 0 auto;
    width: 200px;
}.change_pro_ban_img input {
    display: block;
}.cover_pic_gym .cropit-preview {
    height: 212px !important;
    width: 1200px !important;
}.update_imgsaa {
    float: left;
    text-align: center;
    width: 100%;
}.change_pro_ban_img button[type="submit"] {
    background: #cc3333 none repeat scroll 0 0;
    border: 0 none;
    color: #fff;
    font: 14px "open_sanssemibold";
    margin: 10px auto;
    padding: 7px 20px;
    text-transform: uppercase;
    width: 100px;
}
    </style>    <!-- Custom Theme files -->
    <!----webfonts---->
    <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
     @yield('style')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('front.profileheader')
    @include("layouts.lightboxhtml")
    <?php 
        if(file_exists(public_path('images')."/user_cover_images/".Auth::user()->cover_pic) && Auth::user()->cover_pic != ''){
            $coverpath = URL::asset('public/images/user_cover_images/'.Auth::user()->cover_pic.'');
        }else {
            
            //echo url().'/public/images/user_profile_images/'.Auth::user()->profile_pic;
            $coverpath = URL::asset('assets/images/profile-bg.jpg');
        } 
    ?>  
    <div class="main-page profile">
        <div style='background: rgba(0, 0, 0, 0) url("{{$coverpath}}") no-repeat scroll center center / cover ;' class="title-bar">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="profile-section">
              <div class="change-cover"><a class="set_img_cvr cover_img_change logo_icon" href="#cover_img_change"><i class="fa fa-camera"></i> Change Cover</a></div>
              <div class="profile-doc">
                <?php 
                    if(file_exists(public_path('images')."/user_profile_images/".Auth::user()->profile_pic) && Auth::user()->profile_pic != ''){
                        $path = URL::asset('public/images/user_profile_images/'.Auth::user()->profile_pic.'');
                    }else {
                        //echo url().'/public/images/user_profile_images/'.Auth::user()->profile_pic;
                        $path = URL::asset('assets/images/profile02.jpg');
                    } 
                ?>
                
                <div class="profile-image"> <img id="upload_userlogo" src="{{ $path }}" alt="" /> <a class="profile_change logo_icon" href="#pro_img_change" ><i class="fa fa-camera"></i></a></div>
                <div class="profile-text">
                  <div class="profile-title"> {{ucfirst(Auth::user()->name)}} {{Auth::user()->last_name}}</div>
                  <div class="profile-designation"> {{ucfirst(Auth::user()->role_type)}}, Member since {{date('M Y',strtotime(Auth::user()->created_at))}}</div>
                </div>
              </div>
              <div class="profile-like">
                <div class="views"><a href="#"><i class="fa fa-eye"></i> 172</a></div>
                <div class="likes"><a href="#"><i class="fa fa-thumbs-up"></i> 12</a></div>
                <div class="follows"><a href="#"><i class="fa fa-plus"></i> 123</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   @yield('breadcrumb')
  
  <section>
    <div class="container">
      <div class="row">
        @include('front.profilenav')
        
        <div class="col-xs-9">
         @yield('content')
         
        </div>
      </div>
    </div>
  </section>
</div>
    
   @include('front.footer')
     
    <!-- Scripts -->
    @yield('script')
</body>
</html>
