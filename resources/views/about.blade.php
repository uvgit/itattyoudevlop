@extends('layouts.innermaster')
@section('content')
<div class="title-bar">
    <h2>Dream as if you'll live forever.</h2>
    <p>Live as if you'll die today.</p>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">About us</li>
        </ol>
        <h1 class="page-heading">About Us</h1>
      </div>
    </div>
  </div>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-xs-5"><img class="img-responsive" src="{{ URL::asset('assets/images/about-jpg.jpg') }}" alt="" /></div>
         <div class="col-xs-7">
         <h2 class="sub-heading">Neque porro quisquam </h2>
         <p>Consectetur adipiscing elit. Proin suscipit ex sed est egestas eleifend. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quis elementum lacus. Praesent rutrum ante turpis, id pellentesque mi finibus a. Phasellus pharetra dolor nulla, vitae vehicula massa elementum id. Sed porttitor viverra sem in condimentum. Mauris commodo purus ac nisl tempor aliquam. Aenean sit amet odio pretium, tristique quam at, mattis turpis.</p>

<p>Quisque ut erat ac dolor eleifend imperdiet. Nam nec dui aliquet metus porttitor tincidunt. Nulla porttitor pharetra ante, ut ullamcorper ex accumsan vitae. Proin consequat urna vestibulum turpis pharetra, a ornare metus rutrum. Nunc at imperdiet lorem, vel sollicitudin diam. Integer eget quam nec tellus porttitor porta. Donec pharetra placerat facilisis. Sed eu viverra orci. Fusce laoreet vestibulum viverra. Donec non sollicitudin lorem.</p>

<p>Aliquam fermentum facilisis augue vel finibus. Donec scelerisque ligula velit, sit amet aliquam mauris rhoncus nec. Etiam sollicitudin turpis egestas quam faucibus cursus.</p>
         
         </div>
      </div>
    </div>
  </section>
@endsection
