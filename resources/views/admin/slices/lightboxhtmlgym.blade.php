<!--popup--> 
<script src="{{ URL::asset('assets/front_assets/js/jquery.cropit.js')}}"></script>
<!--light box code-->

<div id="pro_img_change" style="display:none;z-index:9999999;">
    <div class="zoomImagesaveloading loader_alignment">
        <img src="{{url('assets/loader.gif')}}" alt=""/>
    </div>
    <div class="change_pro_ban_img">	

        <form action="#" id="profileImg">

            <div class="uplod_picj_txt">
                Select your Profile Picture (minimum size 200 *200)<br />
            </div>
            <div id="logo-update" class="image-editor logo_update">

                <div class="gym_pro_img change_pro_idd">
                    <input id="logoFile" type='file' class="cropit-image-input"> 
                    <span id='vallogo'></span>
                    <span id='button_logo' style="position: absolute;top: 0;right: 0;background:#2196F3;color: #fff;padding: 9px;">Choose File</span>
                </div>

                <div class="cropit-preview"></div>
                <div class="image-size-label">
                    Resize image
                </div>
                <input type="range" class="cropit-image-zoom-input">
                <input type="hidden" name="image-data" class="hidden-image-data" />
                <div class="update_imgsaa">
                    <button type="submit" class="updateCoverbutton">Update</button>
                </div>
            </div>
        </form>

    </div>	



</div>			

<div id="cover_img_change" style="display:none;z-index:9999999;">
    <div class="zoomImagesaveloading loader_alignment">
        <img src="{{url('assets/loader.gif')}}" alt=""  />
    </div>
    <div class="change_pro_ban_img">	
        <div id="profileImgloader"></div>
        <form action="#" id="profileCover">

            <div class="uplod_picj_txt">
                Select your Cover Pic (minimum size 1200 *360)<br /> 
            </div>
            <div id="cover-update" class="image-editor cover_pic_gym">

                <div class="gym_pro_img change_pro_idd">
                    <input id="coverFile" type='file' class="cropit-image-input"> 
                    <span id='valcover'></span>
                    <span id='button_cover'  style="position: absolute;top: 0;right: 0;background:#2196F3;color: #fff;padding: 9px;">Choose File</span>
                </div>

                <div class="cropit-preview"></div>
                <div class="image-size-label">
                    Resize image
                </div>
                <input type="range" class="cropit-image-zoom-input">
                <input type="hidden" name="image-data" class="hidden-image-data" />
                <div class="update_imgsaa">
                    <button type="submit" class="updateCoverbutton">Update</button>
                </div>

            </div>
        </form>

    </div>	



</div>	

<script>
$(function () {
    $('.zoomImagesaveloading').hide();
    $('#logo-update').cropit({onImageLoading: function () {
            $('.zoomImagesaveloading').show();
            $('.updateCoverbutton').attr('disabled', 'disabled');
            console.log('onImageLoading');
        },
        onImageLoaded: function () {
            $('.zoomImagesaveloading').hide();
            $('.updateCoverbutton').removeAttr('disabled');
            console.log('onImageLoaded');
        },
        onImageError: function (e) {
            if (e.code === 1) {
                console.log('onImageError');
                alert("Please use an image that's at least 200px in width and 200px in height.");

            }
            $('.zoomImagesaveloading').hide();
        },
    });
    $('#cover-update').cropit(
            {onImageLoading: function () {
                    $('.zoomImagesaveloading').show();
                    $('.updateCoverbutton').attr('disabled', 'disabled');
                    console.log('onImageLoading');
                },
                onImageLoaded: function () {
                    $('.zoomImagesaveloading').hide();
                    $('.updateCoverbutton').removeAttr('disabled');
                    console.log('onImageLoaded');
                },
                onImageError: function (e) {
                    if (e.code === 1) {
                        console.log('onImageError');
                        alert("Please use an image that's at least 1200px in width and 360px in height.");

                    }
                    $('.zoomImagesaveloading').hide();
                },
            });

    $('#profileImg').submit(function () {
        $('.zoomImagesaveloading').show();
        // Move cropped image data to hidden input
        $('#profileImgloader').show();
        var imageData = $('#logo-update').cropit('export');

        $('.hidden-image-data').val(imageData);

        // Print HTTP request params
        var formValue = $(this).serialize();
        //$('#result-data').text(formValue);
        var gym_user_id = $('#gym_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: SITE_URL + '/api/1.0/gyms/uploadlogo',
            type: 'post',
            data: {'id': gym_user_id, 'role_type': 'gym', 'imageData': imageData}, success: function (data) {
                if (data) {
                    $('#uploaded_logo_input').val(data.logo);
                    $('#uploaded_logo').attr('src', SITE_URL + '/' + data.logo);
                    $('#uploaded_logo').after('<a class="closebtn deletelogcover" data-action="logo" data-user="gym" data-user_id="' + gym_user_id + '" href="#cover_img_change"></a>')
                    $.fancybox.close();
                    $('.zoomImagesaveloading').hide();
                } else {
                    alert('Try after sometime!');
                    $('.zoomImagesaveloading').hide();
                }
            },
            error: function (err) {
                return false;
            }
        });
        // Prevent the form from actually submitting
        return false;
    });
    $('#profileCover').submit(function () {
        $('.zoomImagesaveloading').show();
        $('#profileImgloader').show();
        var imageData = $('#cover-update').cropit('export');
        var gym_user_id = $('#gym_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: SITE_URL + '/api/1.0/gyms/savecoverGym',
            type: 'post',
            data: {'id': gym_user_id, 'user': 'gym', 'base64Cover': imageData},
            success: function (data) {
                if (data) {
                    $('#uploaded_cover_input').val(data.logo);
                    $('#uploaded_cover').attr('src', SITE_URL + '/' + data.logo);
                    $('#uploaded_cover').after('<a class="closebtn deletelogcover" data-action="cover" data-user="gym" data-user_id="' + gym_user_id + '" href="#cover_img_change"></a>')
                    $.fancybox.close();
                    $('.zoomImagesaveloading').hide();
                } else {
                    alert('Try after sometime!');
                    $('.zoomImagesaveloading').hide();
                }
            },
            error: function (err) {
                alert('Try after sometime!');
                $('.zoomImagesaveloading').hide();
                return false;
            }
        });
        // Prevent the form from actually submitting
        return false;
    });
});
// file upload js on step 3
$(document).ready(function () {

    $('#button_logo').click(function () {
         $('.zoomImagesaveloading').show();  
        $("#logoFile").trigger('click');
    })
    $('#button_cover').click(function () {
          $('.zoomImagesaveloading').show();  
        $("#coverFile").trigger('click');
    })

    $("#coverFile").change(function () {
         $('.zoomImagesaveloading').show();  
        $('#valcover').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
    $("#logoFile").change(function () {
         $('.zoomImagesaveloading').show();  
        $('#vallogo').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
});

$(document).ready(function () {
    $(".profile_change").fancybox({
        maxWidth: 650,
        fitToView: false,
        width: '100%',
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
    $(".cover_img_change").fancybox({
        width: 100,
        maxWidth: 1300,
        fitToView: false,
        width: '100%',
                autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
});
</script>
