<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{url('admin')}}" ><img src="{{URL::asset('assets/images/logo.png')}}" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul style="  margin-top: 24px;" class="nav navbar-nav navbar-right">
		<p class="navbar-text"><span class="label bg-success">{{app()->environment()}}</span></p>
         <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{URL::asset('assets/images/placeholder.jpg')}}" alt="">
                    <span>{{ucwords(Auth::user()->name)}}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i>{{ trans('top-header.profile') }} </a></li>
                    <li class="divider"></li>
                    <li>
                        <div style="margin-left: 16px;">
                            {{ Form::open(array('url' => '/logout')) }}
                            <i class="icon-switch2"></i>
                             {{ Form::submit(trans('top-header.logout'), ['class' => 'btn btn-default btn-flat']) }}
                            {{ Form::close() }}
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
