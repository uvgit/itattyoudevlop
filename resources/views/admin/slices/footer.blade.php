
	<!--<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/pace.min.js') }}"></script>-->
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/js/core/app.js') }}"></script>
         <script type="text/javascript" src="{{ URL::asset('assets/js/bootstrap-multiselect.js') }}"></script>
         <script type="text/javascript" src="{{ URL::asset('assets/js/aes.js') }}"></script>
         <script type="text/javascript" src="{{ URL::asset('assets/js/custom.js') }}"></script>
         <script type="text/javascript" src="{{ URL::asset('assets/front_assets/js/jquery.fancybox.js') }}"></script>
            
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
    <script>
        var SITE_URL = "<?php echo config('global.SITE_URL') ?>";
    </script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
    
function myFunction(zoom) {
     zoomLevel = parseFloat(zoom);
   $('#zoomImage').animate({ zoom: zoomLevel, '-moz-transform': 'scale(' + zoomLevel + ')' },'fast');
}


    function promyFunction(zoom) {
      var zoomLevel = parseFloat(zoom);
   $('canvas').animate({ zoom: zoomLevel, '-moz-transform': 'scale(' + zoomLevel + ')' },'fast');
}
if($( "#prozoomRange" ).length){
        $( "#prozoomRange" ).slider({
          max: 3,
          min: 0.1,
          value:1,
          step: 0.3,
          slide: function( event, ui ) {
              promyFunction(ui.value);
          }

       });
     }
if($( "#zoomRange" ).length){
    $( "#zoomRange" ).slider({
      max: 3,
       min: 0.1,
       value: 1,
       step: 0.3,
       slide: function( event, ui ) {
          myFunction(ui.value);
       }

    });
}
 
 </script>
    
    @yield('script')
    @yield('google-api')
    <!-- Footer -->
   
    <!-- /footer -->
