<!--popup--> 
<script src="{{ URL::asset('assets/front_assets/js/jquery.cropit.js')}}"></script>
<!--light box code-->

<div id="pro_img_change" style="display:none;z-index:9999999;">

    <div class="change_pro_ban_img">	

        <form action="#" id="profileImg">

            <div class="uplod_picj_txt">
                Select your Profile Picture <br /> Note (Kindly upload the image having minimum size 200 *200)
            </div>
            <div id="logo-update" class="image-editor logo_update">

                <div class="gym_pro_img change_pro_idd">
                    <input id="logoFile" type='file' class="cropit-image-input"> 
                    <span id='vallogo'></span>
                    <span id='button_logo'>Choose File</span>
                </div>

                <div class="cropit-preview"></div>
                <div class="image-size-label">
                    Resize image
                </div>
                <input type="range" class="cropit-image-zoom-input">
                <input type="hidden" name="image-data" class="hidden-image-data" />
                <div class="update_imgsaa">
                    <button type="submit">Update</button>
                </div>
            </div>
        </form>

    </div>	



</div>			

<div id="cover_img_change" style="display:none;z-index:9999999;">

    <div class="change_pro_ban_img">	
        <div id="profileImgloader"></div>
        <form action="#" id="profileCover">

            <div class="uplod_picj_txt">
                Select your Cover Pic <br /> Note (Kindly upload the image having minimum size 1200 *360)
            </div>
            <div id="cover-update" class="image-editor cover_pic_gym">

                <div class="gym_pro_img change_pro_idd">
                    <input id="coverFile" type='file' class="cropit-image-input"> 
                    <span id='valcover'></span>
                    <span id='button_cover'>Choose File</span>
                </div>

                <div class="cropit-preview"></div>
                <div class="image-size-label">
                    Resize image
                </div>
                <input type="range" class="cropit-image-zoom-input">
                <input type="hidden" name="image-data" class="hidden-image-data" />
                <div class="update_imgsaa">
                    <button type="submit">Update</button>
                </div>

            </div>
        </form>

    </div>	



</div>	

<script>
$(function () {
    $('#logo-update').cropit();
    $('#cover-update').cropit();

    $('#profileImg').submit(function () {
        // Move cropped image data to hidden input
        $('#profileImgloader').show();
        var imageData = $('#logo-update').cropit('export');

        $('.hidden-image-data').val(imageData);

        // Print HTTP request params
        var formValue = $(this).serialize();
        //$('#result-data').text(formValue);
        var trainer_user_id = $('#trainer_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: SITE_URL + '/api/1.0/gyms/uploadlogo',
            type: 'post',
            data: {'id': trainer_user_id, 'role_type': 'trainer', 'imageData': imageData}, success: function (data) {
                if (data) {
                    $('#uploaded_logo_input').val(data.logo);
                    $('#uploaded_logo').attr('src', SITE_URL + '/' + data.logo);
                    $.fancybox.close();
                } else {
                    alert('Try after sometime!');
                }
            },
            error: function (err) {
                return false;
            }
        });
        // Prevent the form from actually submitting
        return false;
    });
    $('#profileCover').submit(function () {
        $('#profileImgloader').show();
        var imageData = $('#cover-update').cropit('export');
        var trainer_user_id = $('#trainer_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: SITE_URL + '/api/1.0/gyms/savecoverGym',
            type: 'post',
            data: {'id': trainer_user_id, 'user': 'trainer', 'base64Cover': imageData},
            success: function (data) {
                if (data) {
                    $('#uploaded_cover_input').val(data.logo);
                    $('#uploaded_cover').attr('src', SITE_URL + '/' + data.logo);
                    $.fancybox.close();
                } else {
                    alert('Try after sometime!');
                }
            },
            error: function (err) {
                alert('Try after sometime!');
                return false;
            }
        });
        // Prevent the form from actually submitting
        return false;
    });
});
// file upload js on step 3
$(document).ready(function () {

    $('#button_logo').click(function () {
        $("#logoFile").trigger('click');
    })
    $('#button_cover').click(function () {
        $("#coverFile").trigger('click');
    })

    $("#coverFile").change(function () {
        $('#valcover').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
    $("#logoFile").change(function () {
        $('#vallogo').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
});

$(document).ready(function () {
    $(".profile_change").fancybox({
        maxWidth: 650,
        fitToView: false,
        width: '100%',
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
    $(".cover_img_change").fancybox({
        width: 100,
        maxWidth: 1300,
        fitToView: false,
        width: '100%',
                autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
});
</script>
