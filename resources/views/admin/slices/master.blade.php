<html>
    @include('admin.slices.header')

    <body>

        @include('admin.slices.top-menu')


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->

                @include('admin.slices.left-side-navbar-bar')
                <!-- /main sidebar -->
                <!-- main content -->
                <div class="content-wrapper">
                      @include('admin.slices.breadcrumb')
                    <!-- Content area -->
                    <div class="content">
                        @yield('content')
                    </div>
                    <!-- /content area -->
                    @include('admin.slices.copyrights')
                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        @include('admin.slices.footer')
    </body>
</html>
