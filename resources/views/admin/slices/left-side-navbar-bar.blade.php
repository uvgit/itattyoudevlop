<!-- Main navigation -->

<div class="sidebar sidebar-main">
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="{{ URL::asset('assets/images/placeholder.jpg') }}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{ucwords(Auth::user()->name)}}</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-mail5 text-size-small"></i> &nbsp;{{ucwords(Auth::user()->email)}}
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <!--  <li>
                                  <a href="#"><i class="icon-cog3"></i></a>
                              </li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div> 
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li>
                        <a href=""><i class="icon-comments"></i> <span>Ebook</span></a>
                    </li>
                    

                    <!-- /page kits -->

                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>

