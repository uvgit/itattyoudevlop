<div class="page-header">


    <div class="breadcrumb-line breadcrumb-line-component content-group-sm">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">@yield('page_name')</li>
        </ul>


    </div>
</div>