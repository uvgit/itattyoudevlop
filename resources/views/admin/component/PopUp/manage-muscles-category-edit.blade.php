
<div class="modal-dialog" >

    <div class="modal-content">
        <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
            <button type="button" class="close" data-dismiss="modal" modal ng-click="close()">&times;</button>
            <h6 class="modal-title">Muscles Category Edit</h6>
        </div>
      
        <div ng-show="loadingArray.edit[postEditData._id]" >  <i class="icon-spinner2 spinner" ></i></div>
        <div class="modal-body" ng-show="!loadingArray.edit[postEditData._id]" ng>
            <p class="alert alert-success" ng-bind="responseMessage.message" ng-if="responseMessage.message">
            @{{ responseMessage.message}}
        </p>
            <fieldset class="content-group">
                <form  ng-submit="MusclesCategoryEdit()" novalidate>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Muscles Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" ng-model="postEditData.muscles_category_name"  ng-value="postEditData.muscles_category_name" placeholder="Muscles Name"  ng-maxlength="25" required>
                            (Max size 25 characters)
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Short Description </label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" ng-model="postEditData.muscles_category_shot_description" ng-value="postEditData.muscles_category_shot_description" placeholder="Short Description" >
                            (Max size 50 characters)
                        </div>
                    </div>



                    <div class="text-right">
                        <button class="btn btn-primary" ng-click="close()">
                            Close 
                        </button>
                        <button  ng-if="!loadingArray.postedit" type="submit" ng-model="postEditData.submit" class="btn btn-primary">
                            Create  <i class="icon-arrow-right14 position-right"></i>
                        </button>

                        <button ng-if="loadingArray.postedit"  disabled="" class="btn btn-primary">
                            <i class="icon-spinner2 spinner" ></i>

                            <span> @{{ postLoaderText}} </span>
                        </button>
                    </div>
                </form>
            </fieldset>  
        </div>

    </div>
</div>
