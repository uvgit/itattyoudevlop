<!--thumbnail light box-->	
<div id="get_desi_thum" style="display:none;z-index:9999999;" ng-app="fileUpload" ng-controller="MyCtrl" >

    <div class="change_pro_ban_img">	
        <img ng-if="thumbnailloading" src="{{ url('assets/front_assets/images/load.gif')}}">
        <form action="#" onsubmit="return false">
            <div class="select_thumbnail_mob">
                <ul>
                    <li  ng-repeat="thumbselect in thumb_select track by $index">
                        <div class="ligh_thm_img">
                            <img ng-click="selectImages(thumbselect.thumb,thumbselect.original,$index)" ng-src="@{{ thumbselect.thumb|URL}}" alt="" />
                        </div>
                        <div class="sle_rdo">
                            <input type="radio" name="thumbselect" ng-value="@{{ $index}}" ng-model="thumbselectradio" ng-checked="(thumbselectradio==$index)" ng-click="selectImages(thumbselect.thumb,thumbselect.original,$index)">
                        </div>
                    </li>
                  
                </ul>
            </div>
          
            <div class="uplod_picj_txt">
                Choose Custom Thumbnail
            </div>
            <div class="image-editor cover_pic_gym">

                <div class="gym_pro_img change_pro_idd chsthum">
                    <input type='file' id="fileUpload" class="cropit-image-input" ng-model-instant onchange="angular.element(this).scope().imageUpload(this)" />
                   
                    <span id='val'></span>
                    <span id='button_video_thumbnail' >Choose File</span>
                </div>
                <div class="update_imgsaa hfbgcd">
                    <button type="submit" ng-click="thumbnailUpdate()">Update</button>
                </div>
            </div>
        </form>
    </div>	
</div>
<script>
$('#button_video_thumbnail').on('click',function(){
    $('#fileUpload').click();
})
</script>