@extends('admin.slices.master')
@section('title', 'Dashboard')
@section('content')
@include("admin.component.flash")
<!-- Main content -->
<!-- Main charts -->
<div class="row">
    <div class="col-lg-7">

        <!-- Traffic sources -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Traffic sources</h6>
                <div class="heading-elements">
                    <form class="heading-form" action="#">
                        <div class="form-group">
                            <label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
                                <input type="checkbox" class="switch" checked="checked">
                                Live update:
                            </label>
                        </div>
                    </form>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <ul class="list-inline text-center">
                            <li>
                                <a href="#" class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-plus3"></i></a>
                            </li>
                            <li class="text-left">
                                <div class="text-semibold">New visitors</div>
                                <div class="text-muted">2,349 avg</div>
                            </li>
                        </ul>

                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="content-group" id="new-visitors"></div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <ul class="list-inline text-center">
                            <li>
                                <a href="#" class="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-watch2"></i></a>
                            </li>
                            <li class="text-left">
                                <div class="text-semibold">New sessions</div>
                                <div class="text-muted">08:20 avg</div>
                            </li>
                        </ul>

                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="content-group" id="new-sessions"></div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <ul class="list-inline text-center">
                            <li>
                                <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-people"></i></a>
                            </li>
                            <li class="text-left">
                                <div class="text-semibold">Total online</div>
                                <div class="text-muted"><span class="status-mark border-success position-left"></span> 5,378 avg</div>
                            </li>
                        </ul>

                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="content-group" id="total-online"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="position-relative" id="traffic-sources"></div>
        </div>
        <!-- /traffic sources -->

    </div>

</div>
<!-- /main charts -->

<!-- /main content -->

@endsection
@section('script')
<!-- Theme JS files -->
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/moment/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/daterangepicker.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/dashboard.js') }}"></script>
	<!-- /theme JS files -->
@endsection
