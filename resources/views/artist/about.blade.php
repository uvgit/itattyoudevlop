@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">About</li>
        </ol>
        <h1 class="page-heading">About</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")
<!--    This alert div are used for ajax response -->
    <p  style="display:none" class="alert alert-success"></p>
     <p  style="display:none" class="alert alert-danger"></p>
<!--end-->

 <table class="table profile_table">
                <tbody>
                  <tr>
                    <td>
                        <form class="profile_form" role="form" method="post" action="{{ url('/artist/addartist') }}">
                            {{ csrf_field() }}
                        <div class="form-group">
                          <textarea class="form-control textarea"  required="" rows="3" name="about" id="Message" placeholder="e.g. I am a freelance artist etc." data-bv-field="Message">{{ @$get_artist->about }}</textarea>
                        </div>
                        <div class="form-group">
                            <input required="" name="experience"  class="form-control datepicker" placeholder="Working Experience Years" value="{{ @$get_artist->experience }}" type="text">
                        </div>
                        <div class="form-group">
                          <div class="radioBtn btn-group" > <a class="btn btn-default btn-lg <?php if(@$get_artist->gender == 'M' || @$get_artist->gender == '' ) echo "Active active";  ?>" data-toggle="happy" data-title="M">Male</a> <a class="btn btn-default btn-lg <?php if(@$get_artist->gender == 'F') { echo "Active active";}else { echo "notActive";}  ?>" data-toggle="happy" data-title="F">Female</a> </div>
                          <input type="hidden" value="<?php if(count($get_artist) > 0){ echo $get_artist->gender; }else{ 'M'; }?>" name="gender" id="happy">
                        </div>
                        <div class="form-group">
                            <input required="" name="birthday" class="form-control datepicker1" placeholder="Your Birthday in yyyy-mm-dd format" value="{{ @$get_artist->birthday }}" type="text">
                        </div>
                        <div class="row">
                          <div class="col-xs-6">
                            <div class="form-group">
                            <label>Search Address</label>
                            <input name="search_address"   required="" class="form-control" onfocus="geolocate()" class="form-control" id="autocomplete"  placeholder="Work Location" value="{{ @$get_artist->search_address }}" type="text" placeholder="Search Address" value="" type="text">
                              <input value="{{ @$get_artist->lat }}|{{ @$get_artist->long }}" name="latlong" id="latlong" type="hidden">
                            </div>
                            <div class="form-group">
                              <label>Building Address</label>
                              <input class="form-control" name="building_address" required="" placeholder="Building Address" value="{{ @$get_artist->building_address }}" type="text">
                            </div>
                            <div class="form-group">
                            <label>Nearest Landmark</label>
                            <input class="form-control" name="landmark"  required="" placeholder="Nearest Landmark" value="{{ @$get_artist->landmark }}" type="text">
                            </div>
                          </div>
                          <div class="col-xs-6">
                            <div class="form-group">
                              <label>Street</label>
                              <input readonly required="" id="street_number" name="street_number" class="form-control" placeholder="Street" value="{{ @$get_artist->street_number }}" type="text">
                            </div>
                            <div class="row">
                              <div class="col-xs-6">
                                <div class="form-group">
                                 <label>City</label>
                                  <input readonly id="city" name="city" class="form-control" placeholder="City" value="{{ @$get_artist->city }}" type="text">
                                </div>
                              </div>
                              <div class="col-xs-6">
                                <div class="form-group">
                                  <label>State</label>
                                  <input readonly required="" name="state" id="administrative_area_level_1" class="form-control" placeholder="State" value="{{ @$get_artist->state }}" type="text">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-6">
                                <div class="form-group">
                                <label>Country</label>
                                <input readonly required="" name="country" id="country" class="form-control" placeholder="Country" value="{{ @$get_artist->country }}" type="text">
                                </div>
                              </div>
                              <div class="col-xs-6">
                                <div class="form-group">
                                 <label>Pincode</label>
                                 <input readonly required="" id="postal_code" name="postal_code" class="form-control"  placeholder="Pincode" value="{{ @$get_artist->pincode }}" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label>Tattoing Styles</label>
                            @if(count($get_tatto) > 0)
                            @foreach($get_tatto as $tatto)
                            <div class="checkbox">
                                <label>
                                    <input name="tatto_style[]" <?php if(in_array($tatto->id,@$get_tatto_style)) echo "checked"; ?> in type="checkbox" value="{{$tatto->id}}">
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span> {{$tatto->name}}
                                </label>
                            </div>
                            @endforeach
                            @endif

                        </div>
                        <div class="form-group">
                            <input  required="" name="charge_per_hour" class="form-control" placeholder="Charges Per Hour" value="{{ @$get_artist->charge_per_hour }}" type="text">
                        </div>
                        <div class="form-group">
                          <input  required="" name="charge_per_inch" class="form-control" placeholder="Charges Per Inch" value="{{ @$get_artist->charge_per_inch }}" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="website" placeholder="Website URL" value="{{ @$get_artist->website }}" type="text">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-lg pull-left">Save</button>
                        </div>
                      </form></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.datepicker').datepicker({
		format: "yyyy",
		todayHighlight: true,
		autoclose: true,
    });
    $('.datepicker1').datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		autoclose: true,
    });
        
});
</script>
@endsection

