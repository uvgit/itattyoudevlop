@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="{{URL::to("/")}}">Home</a></li>
          <li class="breadcrumb-item active">Portfolio</li>
        </ol>
        <h1 class="page-heading">Portfolio</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")
<style>
    .progress-bar { float: none; background-color: #2bbbad;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
    .progress-div {border:#2bbbad 1px solid;padding:0px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
</style>
<div class="tab-pane" id="portfolio">
    <div class="tattoos_library">
        <form id="deleteportfolio" action="{{URL::to('/delete-portfolio')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="del_id" id="del_id"/>
        </form>
        <ul>
            @if(count(@$userportfolio) > 0)
                @foreach($userportfolio as $portfolio)
               
                    <li class="filter">
                        
                      <div class="library_box"> 
                        <div class="port_edit">
                            <a  data-toggle="tooltip" title="Edit"  href="#"><i class="fa fa-edit"></i></a>
                            <a data-toggle="confirmation" href="javascript:delete_portfolio('{{$portfolio->id}}')"><i class="fa fa-trash"></i></a>
                        </div>
                          <img src="{{URL::asset('public/images/portfolio/'.$portfolio->image_name)}}" alt="">
                        <div class="library_title">By: <strong>{{Auth::User()->name}} {{Auth::User()->last_name}}</strong></div>
        <!--                <div class="library_text">
                          <div class="library_like"><i class="fa fa-thumbs-up"> </i>Like<span class="badge">(75)</span></div>
                          <div class="library_favourite"><i class="fa fa-heart"> </i> Favourite<span class="badge">(75)</span></div>-->
                        </div>
                        <div class="library_social">
                          <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                          </ul>
                        </div>
                    </li>
                  
                @endforeach    
            @endif    
         
          <li class="filter"> <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#addport" class="btn btn-warning btn-lg"><i class="fa fa-plus"></i> Add More </a> </li>
           <div id="addport" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg modal-scroll scrollbar style-4" > 
                    <div class="modal-content">
                        <div class="modal-header"> <a class="close" data-dismiss="modal">&times;</a>
                            <h4 class="modal-title">Help us catalog your work</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                  <a href="javascript:void(0)" class="btn btn-success add_images">Add Images</a><!--   <a class="btn btn-success" href="#">Done</a>-->
                                  <input type="file" style="display: none"  class="addimg"  id="files" name="addimg" multiple="" accept="image/*"/>
                                  <input type="hidden" id="image_count" value="0" />
                                  
                                </div>
                            </div>
                            <div  id='previewImg'></div>
                            </div>
                        <div class="modal-footer"> </div>
                        </div>
                      </div>
                    </div>
        </ul>
    </div>
</div>
<div style="display:none" class="col-md-2" id="tattoo_type">
    <h3> Type<span>*</span></h3>
    <div class="funkyradio">
        <div class="btn-group btn-group-vertical" data-toggle="buttons">
            @foreach($get_tatto_style as $tatto_style)
                <label class="btn">
                    <input type="radio" class="tatto_class" value="{{$tatto_style->id}}"  name='tattoo_type'>
                    <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span> {{$tatto_style->name}}</span> 
                </label>
            @endforeach

        </div>
    </div>
</div>
<div style="display:none" class="col-md-7" id="tattoo_type_category">
    <h3> Category <span> (at least one) </span></h3>
    <div class="funkyradio-group scrollbar style-4">
        @foreach($get_tatto_category as $tatto_category)
            <div class="checkbox">
                <label>
                        <input value="{{$tatto_category->id}}" class="tatto_category" name="tattoo_category[]" type="checkbox">
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span> {{$tatto_category->name}}
                </label>
            </div>
        @endforeach
                                           
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.add_images').click(function(){
            $('input[id=files]').click();
           // return false;
        });
        $('#addport').on('hidden.bs.modal', function () {
            location.reload();
        });
        $(document).on("click",".save_portfolio",function(){
            var i = $(this).attr("id");
            $("#error_"+i).hide();
            $("#success_"+i).hide();
            if(!$('#addimages_'+i+' input[type="radio"]').is(':checked')){
               $("#error_"+i).show().html('Please select tattoo type');
               return false;
            }else if(!$('#addimages_'+i+' input[type="checkbox"]').is(':checked')){
               $("#error_"+i).show().html('Please check at least one category');
               return false;
            }
            $(this).attr("disabled","disabled");
            $(this).html("Please Wait..");
            $.ajax({ 
                type:"POST",
                url:'{{ url('/portfolio-image') }}',
                data:$('#addimages_'+i).serialize(),
                cache: false,
                processData:false,
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                    alert(thrownError);
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            $("#progress-bar_"+i).html(parseInt(percentComplete * 100) + '%').css('width',parseInt(percentComplete * 100) + '%');
                        }
                    }, false);
                    return xhr;
                },
                beforeSend:function() {
                    $("#progress-bar_"+i).html('').css('width','0%');
                },
                success:function (data){
                    
                    if(data == 1) {
                        $("#progress-div_"+i).hide();
                        $("#"+i).hide();
                        $('#addimages_'+i+' input[type="radio"]').attr("disabled","disabled");
                        $('#addimages_'+i+' input[type="checkbox"]').attr("disabled","disabled");
                        $("#update_portfolio_"+i).val(1);
                        $("#success_"+i).show().html('Portfolio added successfully');
                    }else {
                        $("#"+i).removeAttr("disabled","disabled");
                        $("#"+i).html("Save");
                        $("#error_"+i).show().html(data);
                    }
                    
                },error:function(error){
                    $("#error_"+i).show().html('Error. please try again some time!!!');
                    $("#"+i).removeAttr("disabled","disabled");
                    $("#"+i).html("Save");
                }
                
            });
            
            return false;
        });
       
    });

function handleFileSelect(evt) {
    var tatto_type = $("#tattoo_type").html();
    var tatto_type_category = $("#tattoo_type_category").html();
    var files = evt.target.files; // FileList object
    var d = 1;
    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }
        
        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                var img = parseInt($("#image_count").val()) + parseInt(d);
                $("#image_count").val(img);
                var token = '{{ csrf_token() }}';
                // Render thumbnail.
                var div = document.createElement('div');
                 div.innerHTML = ['<div class="addportfolio"><form name="addimages_',img,'" id="addimages_',img,'" enctype="multipart/form-data" ><div class="row">\n\
                                            <input type="hidden" name="update_portfolio" id="update_portfolio_',img,'"/><p  style="display:none;margin-top: 30px;" id="success_',img,'" class="alert alert-success"></p>\n\
                                            <p  style="display:none;margin-top: 30px;" id="error_',img,'" class="alert alert-danger"></p><div class="col-md-3">\n\
                                            <h3>Image</h3><input type="hidden" name="_token" value="',token,'"/><input type="hidden" value="', e.target.result,'" name="tatto_image"/>\n\
                                            <div class="addimage">\n\
                                                <img class="img-thumbnail img-responsive" src="', e.target.result,'" title="', escape(theFile.name), '" >\n\
                                                <center><span>', escape(theFile.name),'</span><br/>\n\
                                                <span>',Math.round(theFile.size/1024) +'  KB</span></center>\n\
                                                <div id="progress-div_',img,'" class="progress-div">\n\
                                                    <div class="progress-bar" id="progress-bar_',img,'">\n\
                                                </div></div>\n\
                                            </div>\n\
                                        </div>\n\
                                        <div class="col-md-2">',tatto_type,'\n\
                                        </div>\n\
                                        <div class="col-md-7">',tatto_type_category,'\n\
                                        </div>\n\
                                        <div class="row">\n\
                                            <div class="col-xs-12">\n\
                                                <button type="submit" id="',img,'"  class="btn btn-success pull-right save_portfolio">Save</a></div> \n\
                                            </div>\n\
                                        </div>\n\
                                </div></form></div>'].join('');
                                                
                document.getElementById('previewImg').insertBefore(div, null);
            };
        })(f);
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        d++;
    }
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);
function delete_portfolio(id){
    $("#del_id").val(id);
    $("#deleteportfolio").submit();
    return false;
}

</script>
@endsection

