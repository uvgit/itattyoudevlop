@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Reviews</li>
        </ol>
        <h1 class="page-heading">Reviews</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")

<div class="tab-pane" id="followers">
    <div class="tattoos_reviews">
        @if(count(@$reviews) > 0) 
            @foreach($reviews as $user_reviews)
                <div class="panel panel-dark">
                    <div class="panel-heading"> 
                        <!-- panel-btns -->
                        <h3 class="panel-title">By <strong>{{$user_reviews->name}} {{$user_reviews->last_name}}</strong> <em>{{App\User::GetDate($user_reviews->created_at)}} ago</em></h3>
                        <div class="rating">
                            @for($i=1; $i<=5 ;$i++)
                                @if($i <= $user_reviews->star)
                                    <span class="fa fa-star"></span> 
                                @else
                                    <span class="fa fa-star-o"></span>
                                @endif
                                
                            @endfor   
                        </div>
                    </div>
                    <div class="panel-body"> 
                      <!-- panel-btns -->
                        <p>{{$user_reviews->review}}</p>
                    </div>
                </div>
            @endforeach
        @else
        @endif

  </div>
</div>
@endsection
@section('script')

@endsection

