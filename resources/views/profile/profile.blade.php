@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Profile</li>
        </ol>
        <h1 class="page-heading">Profile</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")


<!--    This alert div are used for ajax response -->
    <p  style="display:none" class="alert alert-success"></p>
     <p  style="display:none" class="alert alert-danger"></p>
<!--end-->
 <table class="table table-hover profile_table">
            <tbody>
              <tr>
                <th>First Name</th>
                <td id="field_1">{{ucfirst(auth::user()->name)}}</td>
                <td  id="button_1"><a href="javascript:void(0)" class="edit_fields" id="1"><i class="fa fa-edit"></i> Edit </a></td>
              </tr>
              <tr>
                <th>Last name</th>
                <td id="field_2">{{ucfirst(auth::user()->last_name)}}</td>
                <td id="button_2"><a href="javascript:void(0)" class="edit_fields" id="2"><i class="fa fa-edit"></i> Edit </a></td>
              </tr>
              <tr>
                <th>Email-id</th>
                <td id="field_3">{{auth::user()->email}}</td>
                <td id="button_3"><a href="javascript:void(0)" class="edit_fields" id="3"><i class="fa fa-edit"></i> Edit </a></td>
              </tr>
              <tr>
                <th>Contact No.</th>
                <td id="field_4">{{auth::user()->contact_no}}</td>
                <td id="button_4"><a href="javascript:void(0)" class="edit_fields" id="4"><i class="fa fa-edit"></i> Edit </a></td>
              </tr>
              
            </tbody>
          </table>
          <table class="table profile_table">
            <tbody>
              <tr>
                <th>Password</th>
                <td>
                    <form class="profile_form" method="POST" role="form" action="{{ url('/change-password') }}">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <input class="form-control" name="password" required="" placeholder="New Password" type="password">
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="confirm_password" required="" placeholder="Re-New Password" type="password">
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-warning pull-left">Save</button>
                        </div>
                    </form>
                </td>
                <td></td>
              </tr>
            </tbody>
          </table>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '.edit_fields' , function() {
        var id = $(this).attr("id");
        var field_value = $("#field_"+id).html();
        if(id == 1){
            var text_field = "<input type='text' name='name' value='"+field_value+"' text-attr='1' class='form-control' />";
            var save_button = "<input class='btn btn-warning save_field' type='button' value='save'  id='name'>"; 
        }else if(id == 2){
            var text_field = "<input type='text' name='last_name' value='"+field_value+"' text-attr='2' class='form-control' />";
            var save_button = "<input  class='btn btn-warning save_field' type='button' value='save'  id='last_name'>"; 
        }else if(id == 3){
            var text_field = "<input type='text' name='email' value='"+field_value+"' text-attr='3' class='form-control' />";
            var save_button = "<input class='btn btn-warning save_field' type='button' value='save' id='email'>"; 
        }else if(id == 4){
            var text_field = "<input type='text' name='contact_no' value='"+field_value+"' text-attr='4' class='form-control' />";
            var save_button = "<input class='btn btn-warning save_field'  type='button' value='save' id='contact_no'>"; 
        }
        $("#field_"+id).html(text_field);
        $("#button_"+id).html(save_button);
        $(".save_field").on("click",function(){
            if($(this).val() != 'Wait..') {
                $(".alert-danger").hide();
                $(".alert-success").hide();
                var name = $(this).attr("id");
                var id =$('input[name='+name+']').attr("text-attr");
                var field_value = $('input[name='+name+']').val();
                if($.trim(field_value) != '') {
                    $(this).val('Wait..');
                    $.ajax({
                       type:"POST",
                       url:"{{ url('/update-info') }}",
                       data:{name:field_value,_token:'{{ csrf_token() }}',field_update:name},
                       success:function(data){
                           $(".save_field").val('save');
                           if(data == 1){
                               $(".alert-danger").show().html("Email already exists");
                           }else if(data == 0){
                              $(".alert-danger").show().html("Some error occurred please try again later.");  
                           }else {
                               $(".alert-success").show().html("Updated Successfully."); 
                               $("#field_"+id).html(data);
                               $("#button_"+id).html('<a href="javascript:void(0)" class="edit_fields" id="'+id+'"><i class="fa fa-edit"></i> Edit </a>')
                           }
                       },error:function(error){

                       }
                    });
                }
            }
        });
        return false;
    });
});
</script>
@endsection

