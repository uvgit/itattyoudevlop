@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Followers</li>
        </ol>
        <h1 class="page-heading">Followers</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")

<div class="tab-pane" id="followers">
    <div class="tattoos_library">
        
        @if(count($followers) > 0)
            <ul>
                @foreach($followers as $follow)
                    <?php 
                        if(file_exists(public_path('images')."/user_profile_images/".$follow->profile_pic) && $follow->profile_pic != ''){
                            $path = URL::asset('public/images/user_profile_images/'.$follow->profile_pic.'');
                        }else {
                            $path = URL::asset('assets/images/profile02.jpg');
                        } 
                    ?>
                    <li class="filter">
                      <div class="library_box"> <img src="{{$path}}" alt="">
                        <div class="library_title">By: <strong>{{$follow->name}} {{$follow->last_name}}</strong></div>
                      </div>
                    </li>
                @endforeach    
            </ul>
        @else
            <p class="alert alert-danger">{{Auth::user()->name}} {{Auth::user()->last_name}} doesn't have any followers!!</p>
        @endif
    </div>
</div>
@endsection
@section('script')

@endsection

