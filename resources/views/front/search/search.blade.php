<div class="main-page tattoo-artists">
  <div class="title-bar">
    <div class="container">
      <div class="row">
        <div class="col-xs-7">
          <h1>I Tatt <span>Search</span></h1>
          <p>I Tatt You ensures all artists listed are genuine.</p>
        </div>
        <form id="search-main" action="{{URL::to('/search')}}" method="post" onsubmit="return false;" class="has-validation-callback">
            <div class="col-xs-5">
              <div class="input-group adv-search">
                <input class="form-control" id="autocomplete"  name="search_address" placeholder="Search artists & studios by location..." onfocus="geolocate()" autocomplete="off" type="text">
                <input value="" name="street_number" id="street_number" class="form-control autodata disable" readonly="readonly" type="hidden">
                <input value="" name="city" id="city" readonly="readonly" class="autodata disable" type="hidden">
                <input value="" name="state" id="administrative_area_level_1" readonly="readonly" class="autodata disable" type="hidden">
                <input value="" name="country" id="country" readonly="readonly" class="autodata disable" type="hidden">
                <input value="" name="pincode" id="postal_code" readonly="readonly" class="autodata disable" type="hidden">
                <input value="" name="latlong" id="latlong" type="hidden">
                <input value="artist" name="cat" id="cat" type="hidden">
                <div class="input-group-btn">
                  <div class="btn-group" role="group">
                    <button type="button"  id="search_submit"  value="" onclick="search(this)" class="btn btn-warning"><span class="fa fa-search" aria-hidden="true"></span></button>
                  </div>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>