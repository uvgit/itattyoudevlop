<ul class="nav navbar-nav navbar-right" ng-app="validationApp">
      <li><a href="#" class="btn btn-success">Request an Appiontment</a></li>
    @if (Auth::check()) :
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img width="20" height="20" src="{{ URL::asset('assets/images/logo.png') }}" alt="" /> <?php echo Auth::user()->name; ?>
            <span class="caret"></span></a>
            <ul class="dropdown-menu drop-form default">
                @if(Auth::user()->role_type == 'admin')
                    <li><a href="{{URL::to('/admin')}}">Dasboard</a></li>
                @else
                    <li><a href="{{URL::to('/'.Auth::user()->role_type.'/profile')}}">Profile</a></li>
                @endif
              
              <li>{{ Form::open(array('url' => '/logout')) }}
                            <i class="icon-switch2"></i>
                             {{ Form::submit(trans('top-header.logout'), ['class' => 'btn btn-default btn-flat']) }}
                            {{ Form::close() }}</li>
            </ul>
        </li>
    @else :   
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Register <span class="caret"></span></a>
        <ul class="dropdown-menu drop-form register">
          <li>
            <div class="row">
              <div class="col-md-12"  ng-controller="loginController">
              <h2>Member Registration</h2>
              <p id="error_msg" ng-show="myvalue" ng-bind-html="myvalue" class="alert alert-danger" ></p>
                <form class="form" name="userForm" method="POST" ng-submit="submitForm()" novalidate>
                    <input type="hidden" name="ajax" ng-model="user.ajax" value="1" ng-init="user.ajax=1" />
                    <input type="hidden" name="_token" ng-init="user._token='{{ csrf_token() }}'" ng-model="user._token">
                    <div class="form-group">
                        <div id="radioBtn" class="btn-group">
                            <a class="btn btn-warning btn-md active" data-toggle="fun" data-title="artist">Artist</a>
                            <a class="btn btn-warning btn-md notActive" data-toggle="fun" data-title="studio">Studio</a>
                            <a class="btn btn-warning btn-md notActive" data-toggle="fun" data-title="tattoo-lover">Tattoo Lover</a>
                        </div>
                        <input name="role_type" ng-model="user.role_type" id="fun" ng-init="user.role_type='artist'" value="artist" type="hidden">
                    </div>
                
                <div class="row">
              <div class="col-md-6"><div class="form-group" ng-class="{ 'has-error' : userForm.name.$invalid && !userForm.name.$pristine }">
                    <span class="fa fa-user"></span>
                    <input class="form-control" name="name" placeholder="First Name" ng-model="user.name" required type="text">
                    <p ng-show="userForm.name.$invalid && !userForm.name.$pristine" class="help-block">You first name is required.</p>
                  </div>
              </div>
               <div class="col-md-6"><div class="form-group" ng-class="{ 'has-error' : userForm.last_name.$invalid && !userForm.last_name.$pristine }">
                     <span class="fa fa-key"></span>
                     <input class="form-control" name="last_name" ng-model="user.last_name" placeholder="Last Name" required type="text">
                     <p ng-show="userForm.last_name.$invalid && !userForm.last_name.$pristine" class="help-block">You last name is required.</p>
                  </div>
              </div>
              </div>
                
                <div class="row">
              <div class="col-md-6"><div class="form-group" ng-class="{ 'has-error' : userForm.email.$invalid && !userForm.email.$pristine }" >
                    <span class="fa fa-envelope"></span>
                    <input ng-model="user.email"  class="form-control" placeholder="E-mail Address" name="email" required type="email">
                     <p ng-show="userForm.email.$invalid && !userForm.email.$pristine" class="help-block">Enter a valid email.</p>
                  </div>
              </div>
               <div class="col-md-6"><div class="form-group" ng-class="{'has-error': userForm.contact_no.$error.number}">
                     <span class="fa fa-mobile-phone"></span>
                     <input class="form-control" ng-model="user.contact_no" ng-minlength="10" ng-maxlength="10"   placeholder="Contact No." name="contact_no" required type="text">
                     <p ng-show="userForm.contact_no.$invalid && userForm.contact_no.$error.number" class="help-block">Valid contact no is required.</p>
                     <p ng-show="((userForm.contact_no.$error.minlength || userForm.contact_no.$error.maxlength) && userForm.contact_no.$dirty)" class="help-block"> Contact no. should be 10 digits.</p>
                </div>
              </div>
              </div>
                 
                 <div class="row">
              <div class="col-md-6">
                  <div class="form-group" ng-class="{ 'has-error' : userForm.password.$invalid && !userForm.password.$pristine }">
                    <span class="fa fa-user"></span>
                    <input class="form-control" ng-model="user.password"  placeholder="Password"  name="password" required type="password">
                    <p ng-show="userForm.password.$invalid && !userForm.password.$pristine" class="help-block">Please enter the password.</p>
                    
                  </div>
              </div>
               <div class="col-md-6"><div class="form-group" ng-class="{ 'has-error' : userForm.password_confirmation.$invalid && !userForm.password_confirmation.$pristine }">
                    <span class="fa fa-key"></span>
                    <input class="form-control" ng-model="user.password_confirmation" placeholder="Re-Password" name="password_confirmation"  required type="password">
                    <p ng-show="userForm.v.$invalid && !userForm.password_confirmation.$pristine" class="help-block">Please enter the Re-password.</p>
                  </div>
              </div>
              </div>
                  
                  
                  
                  
                  
                  
                  
                  <div class="checkbox">
          <label>
            <input type="checkbox" value="" >
            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
            By signing up, i agree to <a href="">Terms</a> and <a href="">Privacy Policy</a>
          </label>
        </div>
                  <div class="form-group">
                      <button id="login_btn" ng-disabled="userForm.$invalid" type="submit" class="btn btn-warning btn-block">Register Now</button>
                      <div class="help-block text-right">Already have an account? <a href="">Login</a></div>
                  </div>
                  
                </form>
   
              </div>
            </div>
          </li>
        </ul>
      </li>
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Log in <span class="caret"></span></a>
        <ul class="dropdown-menu drop-form">
          <li>
            <div class="row"  ng-controller="userloginController" >
              <div class="col-md-12"><h2>Member Login</h2>
                <p id="error_msg_login" ng-show="myvalue" ng-bind-html="myvalue" class="alert alert-danger" ></p>
                <ul class="social-buttons">
                <li> <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> </a></li>
                 <li><a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> </a> </li>
                <li> <a href="#" class="btn btn-gp"><i class="fa fa-google-plus"></i></a></li>
                <li>  <a href="#" class="btn btn-li"><i class="fa fa-linkedin"></i></a></li>
                 
                 
                 </ul>
               <div class="divider-form"> <span>Or</span> </div>
               <form class="form" name="loginForm" accept-charset="UTF-8"  ng-submit="loginsubmitForm()" novalidate>
                   <input type="hidden" name="ajax" ng-model="userlogin.ajax" value="1" ng-init="userlogin.ajax=1" />
                   <input type="hidden" name="_token" ng-init="userlogin._token='{{ csrf_token() }}'" ng-model="userlogin._token">
                  <div class="form-group" ng-class="{ 'has-error' : loginFrom.email.$invalid && !loginFrom.email.$pristine }">
                    <span class="fa fa-user"></span>
                    <input class="form-control" id="exampleInputEmail2" ng-model="userlogin.email"  placeholder="E-mail Address" name="email" required type="email">
                    <p ng-show="loginForm.email.$invalid && !loginForm.email.$pristine" class="help-block">Enter a valid email.</p>
                  </div>
                  <div class="form-group" ng-class="{ 'has-error' : loginForm.password.$invalid && !loginForm.password.$pristine }">
                     <span class="fa fa-key"></span>
                     <input class="form-control" id="exampleInputPassword2" ng-model="userlogin.password" name="password" placeholder="Password" required type="password">
                     <p ng-show="loginForm.password.$invalid && !loginForm.password.$pristine" class="help-block">Please enter the password.</p>
                  <div class="checkbox">
          <label>
            <input type="checkbox" value="" >
            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
            Stay signed in
          </label>
        </div>
                 
                   
                  </div>
                  <div class="form-group">
                      <button type="submit" ng-disabled="loginForm.$invalid" id="login_user" class="btn btn-warning btn-block">Log in</button>
                      <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                  </div>
                  
                </form>
              </div>
              <div class="bottom text-center"> Not a registered user yet? <a href="#"><b>Sign Up Now</b></a> </div>
            </div>
          </li>
        </ul>
      </li>
    @endif;  
      <li><a href="#"><span onclick="openNav()" style="font-size:24px;cursor:pointer">&#9776;</span></a></li>
    </ul>