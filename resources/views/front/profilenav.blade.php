<div class="col-xs-3">
    <div class="sidebar-profile">
        <ul>
            @if(in_array('profile',config('globle.'.Auth::user()->role_type.'')))
                <li><a class="<?php if(@$profileactive != '') echo 'active';?>" href="{{URL::to('/'.Auth::user()->role_type.'/profile')}}"><i class="fa fa-user"></i>Profile</a></li>
            @endif
            @if(in_array('about',config('globle.'.Auth::user()->role_type.'')))
                <li><a class="<?php if(@$aboutactive != '') echo 'active';?>"  href="{{URL::to('/'.Auth::user()->role_type.'/about')}}"><i class="fa fa-info-circle"></i>About</a></li>
            @endif
            @if(in_array('portfolio',config('globle.'.Auth::user()->role_type.'')))
                <li><a  class="<?php if(@$portfolioactive != '') echo 'active';?>"  href="{{URL::to('/'.Auth::user()->role_type.'/portfolio')}}"><i class="fa fa-building-o"></i>Portfolio</a></li>
            @endif
            @if(in_array('studio',config('globle.'.Auth::user()->role_type.'')))
                <li><a href="#"><i class="fa fa-building-o"></i>Studios</a></li>
            @endif
            @if(in_array('artist',config('globle.'.Auth::user()->role_type.'')))
                <li><a href="#"><i class="fa fa-building-o"></i>Artist</a></li>
            @endif
            @if(in_array('followers',config('globle.'.Auth::user()->role_type.'')))
                <li><a class="<?php if(@$followersactive != '') echo 'active';?>" href="{{URL::to('/'.Auth::user()->role_type.'/followers')}}"><i class="fa fa-user"></i>Followers </a></li>
            @endif
            @if(in_array('following',config('globle.'.Auth::user()->role_type.'')))
                <li><a class="<?php if(@$followingactive != '') echo 'active';?>" href="{{URL::to('/'.Auth::user()->role_type.'/following')}}"><i class="fa fa-user"></i>Following</a></li>
            @endif
            @if(in_array('reviews',config('globle.'.Auth::user()->role_type.'')))
                <li><a class="<?php if(@$reviewsactive != '') echo 'active';?>" href="{{URL::to('/'.Auth::user()->role_type.'/reviews')}}"><i class="fa fa-info-circle"></i>Reviews</a></li>
            @endif    
        </ul>
    </div>
</div>