<div class="header inner_header fade-in">
  <div class="logo"> <a href="{{URL::to('')}}"><img src="{{ URL::asset('assets/images/logo.png') }}" title="city of clouds" width="110" alt="" /></a> </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    @include('front.login')
  </div>
  <div class="clearfix"> </div>
</div>
@include('front.leftnav')