<div class="footer text-center">
  <div class="container">
    <h1>About Us</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ultrices aliquam purus sit amet condimentum. Ut nisl purus, sollicitudin vitae lorem ac, vulputate interdum risus. Curabitur turpis sapien, sodales id tortor eget, molestie vehicula turpis. Praesent id aliquet sem. Pellentesque lorem quam, laoreet ac ultricies sit amet, tristique non erat. Nunc justo orci, pellentesque non gravida ut, tristique quis metus. Aliquam condimentum enim turpis, a porttitor risus interdum eu. Nunc accumsan eleifend neque eu condimentum.</p>
    <ul class="foter_nav">
      <li><a href="#">Home</a></li>
      <li><a href="#">About Us </a></li>
      <li><a href="#">Our Services</a></li>
      <li><a href="#"> FAQ's</a></li>
      <li><a href="#"> Terms & Conditions</a></li>
      <li><a href="#">Privacy Policy</a></li>
      <li><a href="#">Contact Us</a></li>
    </ul>
    <ul class="foter_social">
      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
    </ul>
    <p class="copy-right">copyright &copy; 2016-17 itattyou. All Rights Reserved </p>
    <p>Made with <i class="fa fa-heart" aria-hidden="true"></i> by P3 Multisolutions</p>
    <a id="back-to-top" href="#" class="btn btn-success btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a> </div>
</div>
<script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script> 
<!--  Bootstrap's JavaScript plugins) --> 
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script> 
<script src="{{ URL::asset('assets/js/bootstrap-datepicker.min.js') }}"></script> 
<script src="{{ URL::asset('assets/js/bootstrap-confirmation.js') }}"></script> 
<script src="{{ URL::asset('assets/common-js/google-api.js') }}"></script> 
<script src="{{ URL::asset('assets/common-js/file-manager.js') }}"></script> 
<script src="{{ URL::asset('assets/front_assets/js/jquery.fancybox.js') }}"></script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWpGCVNDdLjiXLxewuB2zoEz8JD_d_aO4&libraries=places&callback=addressAutocomplete" async defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script> 
<script src="{{ URL::asset('assets/front_assets/js/jquery.cropit.js')}}"></script>
<script  type="text/javascript">

    function search(param) {
        
        if (param.form.search_address.value) {
            localStorage.setItem("street_number", param.form.street_number.value);

            localStorage.setItem("city", param.form.city.value);

            localStorage.setItem("state", param.form.state.value);

            localStorage.setItem("country", param.form.country.value);

            localStorage.setItem("postal_code", param.form.postal_code.value);

            localStorage.setItem("latlong", param.form.latlong.value);

            localStorage.setItem("cat", param.form.cat.value);
            citySearch = (param.form.city.value) ? convertToSlug(param.form.city.value) + '/' : ''
            street_numberSearch = (param.form.street_number.value) ? convertToSlug(param.form.street_number.value) + '/' : ''

            //param.form.action + '/' + city +street_number + param.form.cat.value
            window.location.href = param.form.action + '/' + citySearch + street_numberSearch + param.form.cat.value;
        }
    }
    function convertToSlug(Text)
{
    return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
}
    
</script>
<script>
    var siteurl = 'http://localhost/itattyou';
    var validationApp = angular.module('validationApp', []);
    validationApp.controller('loginController', function($scope,$http) {
        $scope.submitForm = function() {
            $("#error_msg").html('');
            if ($scope.userForm.$valid) {
                $("#login_btn").attr("disabled","disabled");
                $("#login_btn").html("Please Wait..");
                $scope.formData = {};
                $scope.user.role_type = $("#fun").val();
                $http({
                    method  : 'POST',
                    url     : '{{ url('/register') }}',
                    data    : $.param($scope.user),  // pass in data as strings
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                }).then(function successCallback(response) {
                    location.reload();
    
                }, function errorCallback(response) {
                    $("#login_btn").removeAttr("disabled");
                    $("#login_btn").html("Register Now");
                    $scope.myvalue =true;
                    if(response.data.email){
                        $("#error_msg").append(response.data.email+"<br/>");
                    }
                    if(response.data.name){
                        $("#error_msg").append(response.data.name+"<br/>");
                    }
                    if(response.data.last_name){
                        $("#error_msg").append(response.data.last_name+"<br/>");
                    }
                    if(response.data.contact_no){
                        $("#error_msg").append(response.data.contact_no+"<br/>");
                    }
                    if(response.data.password){
                        $("#error_msg").append(response.data.password+"<br/>");
                    }
                });
            }
        };
    });
    //var uservalidationApp = angular.module('validationApp', []);
    validationApp.controller('userloginController', function($scope,$http) {
        $scope.loginsubmitForm = function() {
            $("#error_msg_login").html('');
            if ($scope.loginForm.$valid) {
                $("#login_user").attr("disabled","disabled");
                $("#login_user").html("Please Wait..");
                $scope.formData = {};
                $http({
                    method  : 'POST',
                    url     : '{{ url('/login') }}',
                    data    : $.param($scope.userlogin),  // pass in data as strings
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
                }).then(function successCallback(response) {
                    if(response.data == 'Email and/or password invalid.'){
                        $scope.myvalue =true;
                        $("#login_user").removeAttr("disabled");
                        $("#login_user").html("Log in");
                        $("#error_msg_login").append(response.data);
                    }else {
                        location.reload();
                    }
    
                }, function errorCallback(response) {
                    $("#login_user").removeAttr("disabled");
                    $("#login_user").html("Log in");
                    $scope.myvalue =true;
                    if(response.data.email){
                        $("#error_msg").append(response.data.email+"<br/>");
                    }
                    if(response.data.password){
                        $("#error_msg").append(response.data.password+"<br/>");
                    }
                   
                });
            }
        };
    });
 $(window).load(function() {
   $(".btn-nav").on("click tap", function() {
     $(".nav-container").toggleClass("showNav hideNav").removeClass("hidden");
     $(this).toggleClass("animated");
   });
 });
 $('.carousel').carousel({
  interval: 3000
})
 $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}
$('.radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})
$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    $('#'+tog).attr("ng-init","user.role_type="+sel+"");
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});
$(function () {
    $('.zoomImagesaveloading').hide();
    $('#logo-update').cropit({onImageLoading: function () {
            $('.zoomImagesaveloading').show();
            $('.updateCoverbutton').attr('disabled', 'disabled');
            console.log('onImageLoading');
        },
        onImageLoaded: function () {
            $('.zoomImagesaveloading').hide();
            $('.updateCoverbutton').removeAttr('disabled');
            console.log('onImageLoaded');
        },
        onImageError: function (e) {
            if (e.code === 1) {
                console.log('onImageError');
                alert("Please use an image that's at least 200px in width and 200px in height.");

            }
            $('.zoomImagesaveloading').hide();
        },
    });
    $('#cover-update').cropit(
            {onImageLoading: function () {
                    $('.zoomImagesaveloading').show();
                    $('.updateCoverbutton').attr('disabled', 'disabled');
                    console.log('onImageLoading');
                },
                onImageLoaded: function () {
                    $('.zoomImagesaveloading').hide();
                    $('.updateCoverbutton').removeAttr('disabled');
                    console.log('onImageLoaded');
                },
                onImageError: function (e) {
                    if (e.code === 1) {
                        console.log('onImageError');
                        alert("Please use an image that's at least 1200px in width and 212px in height.");

                    }
                    $('.zoomImagesaveloading').hide();
                },
            });

    $('#profileImg').submit(function () {
        
        var imageData = $('#logo-update').cropit('export');
        
       if(typeof imageData != 'undefined') {
         $(".updateCoverbutton").attr("disabled","disabled");
        $(".updateCoverbutton").html("Uploading..");  
        var formValue = $(this).serialize();
        var gym_user_id = $('#gym_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: '{{ url('/update-logo') }}',
            type: 'post',
            data: {'imageData': imageData}, success: function (data) {
                $(".updateCoverbutton").removeAttr("disabled");
                $(".updateCoverbutton").html("Update");
                if (data != 0) {
                   $("#upload_userlogo").attr("src",siteurl+"/public/images/user_profile_images/"+data+"?var=1");
                   $.fancybox.close();
                } else {
                    alert('Try after sometime!');
                }
            },
            error: function (err) {
                $(".updateCoverbutton").removeAttr("disabled");
                $(".updateCoverbutton").html("Update");
                alert('Try after sometime!');
                return false;
            }
        });
        }
        // Prevent the form from actually submitting
        return false;
   
    });
    $('#profileCover').submit(function () {
        
        var imageData = $('#cover-update').cropit('export');
        
       if(typeof imageData != 'undefined') {
         $(".updateCoverbutton").attr("disabled","disabled");
        $(".updateCoverbutton").html("Uploading..");  
        var formValue = $(this).serialize();
        var gym_user_id = $('#gym_user_id').val();
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
        });
        $.ajax({
            url: '{{ url('/update-cover') }}',
            type: 'post',
            data: {'imageData': imageData}, success: function (data) {
                $(".updateCoverbutton").removeAttr("disabled");
                $(".updateCoverbutton").html("Update");
                if (data != 0) {
                    var imageurl = siteurl+'/public/images/user_cover_images/'+data;
                    $('.title-bar').css('background-image', 'url("'+imageurl+'")');
                    $.fancybox.close();
                } else {
                    alert('Try after sometime!');
                }
            },
            error: function (err) {
                $(".updateCoverbutton").removeAttr("disabled");
                $(".updateCoverbutton").html("Update");
                alert('Try after sometime!');
                return false;
            }
        });
        }
        // Prevent the form from actually submitting
        return false;
   });
    
});
 
// file upload js on step 3
$(document).ready(function () {
     $('[data-toggle="tooltip"]').tooltip();
 $('[data-toggle=confirmation]').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  // other options
});
    $(".profile_change").fancybox({
        maxWidth: 650,
        fitToView: false,
        width: '100%',
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
    $(".cover_img_change").fancybox({
        
       
        fitToView: false,
        width: '1300',
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });

    $('#button_logo').click(function () {
         $('.zoomImagesaveloading').show();  
        $("#logoFile").trigger('click');
    })
    $('#button_cover').click(function () {
          $('.zoomImagesaveloading').show();  
        $("#coverFile").trigger('click');
    })

    $("#coverFile").change(function () {
         $('.zoomImagesaveloading').show();  
        $('#valcover').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
    $("#logoFile").change(function () {
         $('.zoomImagesaveloading').show();  
        $('#vallogo').text(this.value.replace(/C:\\fakepath\\/i, ''))
    })
});


</script>