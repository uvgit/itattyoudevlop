@extends('layouts.searchmaster')
@section('breadcrumb')
<div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ol class="breadcrumb" >
              <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
              <li class="breadcrumb-item active">Search</li>
            </ol>
            <h1 class="page-heading">Search</h1>
          </div>
        </div>
    </div>
@endsection
@section('content')
<h4 class="page-heading">Showing data in <span>@if(@$street != '') {{$street}} @endif</span> <span>@if(@$city != '') {{$city}} @endif</span></h4>
 <div class="col-xs-12">
        <div class="row">
          <div class="col-xs-12"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a class="btn btn-warning" href="#artists" aria-controls="home" role="tab" data-toggle="tab">Artists ({{count($artistdata)}})</a></li>
              <li role="presentation"><a class="btn btn-warning" href="#studios" aria-controls="profile" role="tab" data-toggle="tab">Studios ({{count($studiodata)}})</a></li>
              
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="artists">
                @if(count($artistdata) > 0)    
                    @foreach($artistdata as $fetchartist)
                        <?php 
                            if(file_exists(public_path('images')."/user_profile_images/".$fetchartist->profile_pic) && $fetchartist->profile_pic != ''){
                                $path = URL::asset('public/images/user_profile_images/'.$fetchartist->profile_pic.'');
                            }else {
                                //echo url().'/public/images/user_profile_images/'.Auth::user()->profile_pic;
                                $path = URL::asset('assets/images/profile02.jpg');
                            } 
                        ?>    
                        <section class="artist_box">
                           <div class="row">
                             <div class="col-xs-10">
                               <div class="artist-image"> <img src="{{ $path }}" alt="" /> </div>
                               <div class="artist-text">
                                 <h2>{{$fetchartist->name}} {{$fetchartist->last_name}}<span>Artist</span></h2>
                                 <div class="location"><strong>{{$fetchartist->city}}</strong>, {{$fetchartist->state}}</div>
                                 <div class="address">{{$fetchartist->search_address}}</div>
                                 <ul class="artist_list">
                                   <li><img src="{{ URL::asset('assets/images/artists/artists001.jpg') }}" alt="" /></li>
                                   <li><img src="{{ URL::asset('assets/images/artists/artists002.jpg') }}" alt="" /></li>
                                   <li><img src="{{ URL::asset('assets/images/artists/artists003.jpg') }}" alt="" /></li>
                                   <li><img src="{{ URL::asset('assets/images/artists/artists004.jpg') }}" alt="" /></li>
                                   <li><img src="{{ URL::asset('assets/images/artists/artists005.jpg') }}" alt="" /></li>
                                 </ul>
                                 <ul class="artist_list border0">
                                   <li><a href=""><i class="fa fa-phone"></i> call</a></li>
                                   <li><a href=""><i class="fa fa-building-o"></i> Studio</a></li>
                                   <li><a href=""><i class="fa fa-camera"></i> Portfolio</a></li>
                                   <li><a href=""><i class="fa fa-eye"></i> Reviews</a></li>
                                 </ul>
                               </div>
                             </div>
                             <div class="col-xs-2">
                               <div class="vote_section">
                                 <div class="vote-img">{{$fetchartist->rating}}</div>
                                 <div class="vote-text"><span class="reviews"> {{$fetchartist->rating_count}} reviews</span></div>
                               </div>
                             </div>
                           </div>
                       </section>
                    @endforeach
                @else
                    <p class="alert alert-danger">Artist not found</p>
                @endif     

                </div>
              <div role="tabpanel" class="tab-pane fade" id="studios">
                    @if(count($studiodata) > 0)
                        @foreach($studiodata as $fetchstudio)   
                            <?php 
                                if(file_exists(public_path('images')."/user_profile_images/".$fetchstudio->profile_pic) && $fetchstudio->profile_pic != ''){
                                    $path = URL::asset('public/images/user_profile_images/'.$fetchstudio->profile_pic.'');
                                }else {
                                    $path = URL::asset('assets/images/profile02.jpg');
                                } 
                            ?>  
                            <section class="artist_box">
                                <div class="row">
                                  <div class="col-xs-10">
                                    <div class="artist-image"> <img src="{{ $path }}" alt="" /> </div>
                                    <div class="artist-text">
                                      <h2>{{$fetchstudio->name}} {{$fetchstudio->last_name}}</h2>
                                      <div class="location"><strong>{{$fetchstudio->city}}</strong>, {{$fetchstudio->state}}</div>
                                      <div class="address">{{$fetchstudio->search_address}}</div>
                                      <ul class="artist_list">
                                        <li><img src="{{ URL::asset('assets/images/artists/artists001.jpg') }}" alt="" /></li>
                                        <li><img src="{{ URL::asset('assets/images/artists/artists002.jpg') }}" alt="" /></li>
                                        <li><img src="{{ URL::asset('assets/images/artists/artists003.jpg') }}" alt="" /></li>
                                        <li><img src="{{ URL::asset('assets/images/artists/artists004.jpg') }}" alt="" /></li>
                                        <li><img src="{{ URL::asset('assets/images/artists/artists005.jpg') }}" alt="" /></li>
                                      </ul>
                                      <ul class="artist_list border0">
                                        <li><a href=""><i class="fa fa-phone"></i> call</a></li>
                                        <li><a href=""><i class="fa fa-building-o"></i> Studio</a></li>
                                        <li><a href=""><i class="fa fa-camera"></i> Portfolio</a></li>
                                        <li><a href=""><i class="fa fa-eye"></i> Reviews</a></li>
                                      </ul>
                                    </div>
                                  </div>
                                  <div class="col-xs-2">
                                    <div class="vote_section">
                                      <div class="vote-img">{{$fetchstudio->rating}}</div>
                                      <div class="vote-text"><span class="reviews"> {{$fetchstudio->rating_count}} reviews</span></div>
                                    </div>
                                  </div>
                                </div>
                            </section>
                         @endforeach
                    @else
                        <p class="alert alert-danger">Studios not found</p>
                    @endif    
              </div>
            
            </div>
          </div>
        </div>
      </div>
@endsection
