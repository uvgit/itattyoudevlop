@extends('layouts.profilemaster')
@section('breadcrumb')
<div class="container">
    <div class="row">
      <div class="col-xs-12">
        <ol class="breadcrumb hidden" >
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">About</li>
        </ol>
        <h1 class="page-heading">About</h1>
      </div>
    </div>
  </div>
@endsection
@section('content')
@include("admin.component.flash")
<!--    This alert div are used for ajax response -->
    <p  style="display:none" class="alert alert-success"></p>
    <p  style="display:none" class="alert alert-danger"></p>
<!--end-->

 <table class="table profile_table">
                <tbody>
                  <tr>
                    <td>
                        <form class="profile_form" role="form" method="post" action="{{ url('/studio/addstudio') }}">
                            {{ csrf_field() }}
                        <div class="form-group">
                          <textarea class="form-control textarea"  required="" rows="3" name="about" id="Message" placeholder="e.g.About Your Studio*." data-bv-field="Message">{{ @$get_studio->about }}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <input required="" name="establishment_date" class="form-control datepicker1" placeholder="Establishment Date*" value="{{ @$get_studio->establishment_date }}" type="text">
                        </div>
                        <div class="row">
                          <div class="col-xs-6">
                            <div class="form-group">
                            <label>Studio Location*</label>
                            <input name="search_address"   required="" class="form-control" onfocus="geolocate()" class="form-control" id="autocomplete"  placeholder="Work Location" value="{{ @$get_studio->search_address }}" type="text" placeholder="Search Address" value="" type="text">
                              <input value="{{ @$get_studio->lat }}|{{ @$get_studio->long }}" name="latlong" id="latlong" type="hidden">
                            </div>
                            <div class="form-group">
                              <label>Building Address</label>
                              <input class="form-control" name="building_address" required="" placeholder="Building Address" value="{{ @$get_studio->building_address }}" type="text">
                            </div>
                            <div class="form-group">
                            <label>Nearest Landmark</label>
                            <input class="form-control" name="landmark"  required="" placeholder="Nearest Landmark" value="{{ @$get_studio->landmark }}" type="text">
                            </div>
                          </div>
                          <div class="col-xs-6">
                            <div class="form-group">
                              <label>Street</label>
                              <input readonly required="" id="street_number" name="street_number" class="form-control" placeholder="Street" value="{{ @$get_studio->street_number }}" type="text">
                            </div>
                            <div class="row">
                              <div class="col-xs-6">
                                <div class="form-group">
                                 <label>City</label>
                                  <input readonly id="city" name="city" class="form-control" placeholder="City" value="{{ @$get_studio->city }}" type="text">
                                </div>
                              </div>
                              <div class="col-xs-6">
                                <div class="form-group">
                                  <label>State</label>
                                  <input readonly required="" name="state" id="administrative_area_level_1" class="form-control" placeholder="State" value="{{ @$get_studio->state }}" type="text">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-6">
                                <div class="form-group">
                                <label>Country</label>
                                <input readonly required="" name="country" id="country" class="form-control" placeholder="Country" value="{{ @$get_studio->country }}" type="text">
                                </div>
                              </div>
                              <div class="col-xs-6">
                                <div class="form-group">
                                 <label>Pincode</label>
                                 <input readonly required="" id="postal_code" name="postal_code" class="form-control"  placeholder="Pincode" value="{{ @$get_studio->pincode }}" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label> Facilities Available</label>
                            @if(count($get_studio_facilities) > 0)
                            @foreach($get_studio_facilities as $studio_facilities)
                            <div class="checkbox">
                                <label>
                                    <input name="studio_facilities[]" <?php if(in_array($studio_facilities->id,@$get_user_studio_facilities)) echo "checked"; ?> in type="checkbox" value="{{$studio_facilities->id}}">
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span> {{$studio_facilities->name}}
                                </label>
                            </div>
                            @endforeach
                            @endif

                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-lg pull-left">Save</button>
                        </div>
                      </form></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.datepicker').datepicker({
		format: "yyyy",
		todayHighlight: true,
		autoclose: true,
    });
    $('.datepicker1').datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		autoclose: true,
    });
        
});
</script>
@endsection

