@extends('layouts.frontmaster')
@section('content')
<form id="search-main" action="{{URL::to('/search')}}" method="post" onsubmit="return false;" class="has-validation-callback">
<div class="enqbox">
    <input value="" name="street_number" id="street_number" class="form-control autodata disable" readonly="readonly" type="hidden">
    <input value="" name="city" id="city" readonly="readonly" class="autodata disable" type="hidden">
    <input value="" name="state" id="administrative_area_level_1" readonly="readonly" class="autodata disable" type="hidden">
    <input value="" name="country" id="country" readonly="readonly" class="autodata disable" type="hidden">
    <input value="" name="pincode" id="postal_code" readonly="readonly" class="autodata disable" type="hidden">
    <input value="" name="latlong" id="latlong" type="hidden">
    <input value="artist" name="cat" id="cat" type="hidden">
  <p><span class="line1">Welcome to</span><br>
    <span class="line2"> I tatt You</span></p>
  <p class="line3">Find the best tattoo artist and  tattoo studio around you and avail exclusive offers</p>
  <div class="col-lg-12">
      <div class="input-group adv-search" style="margin:2% 0">
            <input class="form-control" id="autocomplete"  name="search_address" placeholder="Search artists & studios by location..." onfocus="geolocate()" autocomplete="off" type="text">
            
            <div class="input-group-btn">
              <div class="btn-group" role="group">
                <button type="button"  id="search_submit"  value="" onclick="search(this)"  class="btn btn-warning"><span class="fa fa-search" aria-hidden="true"></span></button>
              </div>
            </div>
</div>
  </div>
  
    
  </div>
 
</div>
</form>
<div id="myCarousel" class="carousel slide vertical">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"><span></span></li>
    <li data-target="#myCarousel" data-slide-to="1"><span></span></li>
    <li data-target="#myCarousel" data-slide-to="2"><span></span></li>
    <li data-target="#myCarousel" data-slide-to="3"><span></span></li>
  </ol>
  <!-- Carousel items -->
  <div class="carousel-inner">
    <div class="active item"> <img src="{{ URL::asset('assets/images/bg.jpg') }}" alt=""> </div>
    <div class="item"> <img src="{{ URL::asset('assets/images/bg.jpg') }}" alt=""> </div>
    <div class="item"> <img src="{{ URL::asset('assets/images/bg.jpg') }}" alt=""> </div>
    <div class="item"> <img src="{{ URL::asset('assets/images/bg.jpg') }}" alt=""> </div>
  </div>
</div>
<div id="artist-week">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="artist"> <img src="{{ URL::asset('assets/images/em-week.jpg') }}" alt="" />
          <div class="artist-title">Rocky William</div>
        </div>
      </div>
    </div>
  </div>
</div>

<!----start-top-nav----> 

<!-----start-latest---->
<section id="latest" class="vision">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Latest Tattoo Designs</h1>
        <p>Quisque dui risus, viverra consequat facilisis at, hendrerit eu velit. Aenean ultricies neque eget risus accumsan viverra ut sed ante. Etiam vestibulum lacinia quam vel finibus.</p>
      </div>
      <div class="col-md-12">
        <section id="pinBoot">
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest01.jpg') }}" alt="" /> </article>
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest02.jpg') }}" alt="" /> </article>
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest03.jpg') }}" alt="" /> </article>
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest04.jpg') }}" alt="" /> </article>
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest05.jpg') }}"  alt="" /> </article>
          <article class="white-panel"> <img class="img-responsive" src="{{ URL::asset('assets/images/latest/latest06.jpg') }}"  alt="" /> </article>
        </section>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-latest----> 

<!-----start-Featured ---->
<section class="featured">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Latest Tattoo Designs</h1>
        <p>Quisque dui risus, viverra consequat facilisis at, hendrerit eu velit. Aenean ultricies neque eget risus accumsan viverra ut sed ante. Etiam vestibulum lacinia quam vel finibus.</p>
      </div>
      <div class="col-md-12">
        <div class="row">
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured01.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Lokesh Verma</a></div>
              <div class="featured-designation">Devilz Tattooz, New Delhi</div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured02.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Archana Bhanushali</a></div>
              <div class="featured-designation">Ace Tattooz, Mumbai</div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured03.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Sameer Patange</a></div>
              <div class="featured-designation">Kraayon’z Tattoo Studio,Mumbai</div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured04.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Sunny Bhanushali</a></div>
              <div class="featured-designation">Devilz Tattooz, New Delhi</div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured05.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Niloy Das</a></div>
              <div class="featured-designation">Ace Tattooz, Mumbai</div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="featured-box"> <img class="img-responsive" src="{{ URL::asset('assets/images/featured/featured06.jpg') }}" alt="" />
              <div class="featured-title"><a href="#">Vikas Milani</a></div>
              <div class="featured-designation">Ace Tattooz, Mumbai</div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-Featured ----> 

<!-----start-Events ---->
<section class="events">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Inspirational I Tatt You Videos </h1>
        <p>Quisque dui risus, viverra consequat facilisis at, hendrerit eu velit. <br>
          Aenean ultricies neque eget risus finibus.</p>
        <div class="events-vedio"> <img src="{{ URL::asset('assets/images/event-vedio.png') }}" alt="" />
          <div id="eventsCarousel" class="carousel slide">
            <div class="carousel-inner">
              <div class="item active">
                <iframe src="https://www.youtube.com/embed/IkTw7J-hGmg?controls=0" allowfullscreen="" width="100%" height="325px" frameborder="0"></iframe>
              </div>
              <div class="item">
                <iframe src="https://www.youtube.com/embed/IkTw7J-hGmg?controls=0" allowfullscreen="" width="100%" height="325px" frameborder="0"></iframe>
              </div>
              <div class="item">
                <iframe src="https://www.youtube.com/embed/IkTw7J-hGmg?controls=0" allowfullscreen="" width="100%" height="325px" frameborder="0"></iframe>
              </div>
            </div>
            <a class="left carousel-control" href="#eventsCarousel" data-slide="prev"><i class="fa fa-reply" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#eventsCarousel" data-slide="next"><i class="fa fa-share" aria-hidden="true"></i> </a> </div>
        </div>
      </div>
      <div class="col-md-5">
        <h1>Upcoming Events</h1>
        <p>Viverra consequat facilisis at, hendrerit eu velit. <br>
          Aenean ultricies neque eget risus finibus.</p>
        <div id="events_imgCarousel" class="carousel slide">
          <div class="carousel-inner">
            <div class="item active"> <img src="{{ URL::asset('assets/images/event-slider.jpg') }}" alt="" /> </div>
            <div class="item"> <img src="{{ URL::asset('assets/images/event-slider.jpg') }}" alt="" /> </div>
            <div class="item"> <img src="{{ URL::asset('assets/images/event-slider.jpg') }}" alt="" /> </div>
          </div>
          <a class="left carousel-control" href="#events_imgCarousel" data-slide="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#events_imgCarousel" data-slide="next"><i class="fa fa-chevron-right" aria-hidden="true"></i> </a> </div>
        <div class="events-details"> Pune Tattoo Festival <strong> 14/15/16 October 2016</strong> </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-Events ----> 

<!-----start-cta ---->
<section class="events-cta">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1"> <img src="{{ URL::asset('assets/images/cta-img.jpg') }}" class="pull-left" alt="" />
        <div class="cta-caption">
          <h1> Are you ready to get a tattoo?</h1>
          <p>Quisque dui risus, viverra consequat facilisis at, hendrerit eu velit. Aenean ultricies neque eget risus accumsan viverra ut sed ante. Etiam vestibulum lacinia quam vel finibus.</p>
          <a href="#" class="btn btn-success"> Request an Appointment </a> </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-cta ----> 

<!-----start-testimonials ---->
<section class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1>Testimonials</h1>
        <p>We Love Our Customers And They Meet Us In Return</p>
        <div class="testimonial_item">
          <div id="testimonial_Carousel" class="carousel slide">
            <div class="carousel-inner">
              <div class="item active">
                <div class="testimonial_box"> <img src="{{ URL::asset('assets/images/testimonials01.jpg') }}" alt="" />
                  <div class="test_title"><span>2 days</span> ago by <strong>Teia Naris</strong></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius viverra mauris et efficitur. Pellentesque mauris odio, cursus non sodales in, efficitur et ante. Donec fermentum, urna non tristique faucibus, purus augue faucibus urna, sed fermentum quam justo ut augue. Donec ultrices condimentum sem aliquet vestibulum.</p>
                </div>
              </div>
              <div class="item">
                <div class="testimonial_box"> <img src="{{ URL::asset('assets/images/testimonials01.jpg') }}" alt="" />
                  <div class="test_title"><span>2 days</span> ago by <strong>Teia Naris</strong></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius viverra mauris et efficitur. Pellentesque mauris odio, cursus non sodales in, efficitur et ante. Donec fermentum, urna non tristique faucibus, purus augue faucibus urna, sed fermentum quam justo ut augue. Donec ultrices condimentum sem aliquet vestibulum.</p>
                </div>
              </div>
              <div class="item">
                <div class="testimonial_box"> <img src="{{ URL::asset('assets/images/testimonials01.jpg') }}" alt="" />
                  <div class="test_title"><span>2 days</span> ago by <strong>Teia Naris</strong></div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius viverra mauris et efficitur. Pellentesque mauris odio, cursus non sodales in, efficitur et ante. Donec fermentum, urna non tristique faucibus, purus augue faucibus urna, sed fermentum quam justo ut augue. Donec ultrices condimentum sem aliquet vestibulum.</p>
                </div>
              </div>
            </div>
            <a class="left carousel-control" href="#testimonial_Carousel" data-slide="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#testimonial_Carousel" data-slide="next"><i class="fa fa-chevron-right" aria-hidden="true"></i> </a> </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-testimonials ----> 

<!-----start-Studios ---->
<section class="studios">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="studios_top">
          <h1>Top 10 Tattoo Studios</h1>
          <div id="studios_Carousel" class="carousel slide">
            <div class="carousel-inner">
              <div class="item active"> <img src="{{ URL::asset('assets/images/studios01.jpg') }}" alt="" /> </div>
              <div class="item"> <img src="{{ URL::asset('assets/images/studios01.jpg') }}" alt="" /> </div>
              <div class="item"> <img src="{{ URL::asset('assets/images/studios01.jpg') }}" alt="" /></div>
            </div>
            <a class="left carousel-control" href="#studios_Carousel" data-slide="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#studios_Carousel" data-slide="next"><i class="fa fa-chevron-right" aria-hidden="true"></i> </a> </div>
          <div class="studios_title"><a href="#">Kraayonz Tattoo Studios</a></div>
          <p>Quisque dui risus, viverra consequat facilisis at, hendrerit eu velit. Aenean ultricies neque eget risus accumsan viverra ut sed ante. Etiam vestibulum lacinia quam vel finibus.</p>
        </div>
      </div>
      <div class="col-md-5">
        <div class="quotes">
          <div class="quotes_caption"> <img src="{{ URL::asset('assets/images/quotes.png') }}" alt="" />
            <h1>Tattoo Quotes</h1>
          </div>
          <ul class="quotes_list">
            <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer euismod ligula tellus, sed iaculis libero sagittis fringilla vitae est aliquam commodo. </li>
            <li>Nam sollicitudin tempus metus, dignissim luctus mauris faucibus pretium. Aenean varius ante tellus, nec vestibulum odio fermentum vitae. que nibh felis, egestas non neque nec, lobortis suscipit ante. </li>
            <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer euismod ligula tellus, sed iaculis libero sagittis fringilla vitae est aliquam commodo. </li>
          </ul>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</section>
<!-----//End-Studios ----> 

@endsection
