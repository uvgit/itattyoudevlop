<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        /* if(Auth::check()){
          $user=  \Auth::user()->role_type;
          prd($user);
          } */
    
        $roles = $this->getRequiredRoleForRoute($request->route());
        
       
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        return $next($request);
    }

    private function getRequiredRoleForRoute($route) {
       if (isset(Auth::user()->role_type)) {
            $role = Auth::user()->role_type;
        }
        
        
        return isset($role) ? $role : null;
    }

}
