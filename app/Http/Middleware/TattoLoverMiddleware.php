<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TattoLoverMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        // Get the required roles from the route
   
        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        $role = "";
        if (isset(Auth::user()->role_type)) {
            $role = Auth::user()->role_type;
        }
        if ($role == 'tatto-lover') {
            return redirect()->back();;
        }
        return $next($request);
       
      
    }

}
