<?php

namespace App\Http\Controllers\Admin\Customuploads;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomUploadsController extends Controller
{
    public function showIndex() {
        return view('admin.customupload.index');
    }
    public function showCover() {
       
        return view('admin.customupload.cover_upload');
    }
    public function showLogo() {
        return view('admin.customupload.logo_upload');
    }
}
