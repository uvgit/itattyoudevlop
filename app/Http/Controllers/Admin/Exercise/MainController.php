<?php

namespace App\Http\Controllers\Admin\Exercise;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function slugByCounter($center_name) {

        $center_name = preg_replace('/[^A-Za-z0-9 \-]/', '', $center_name);
        $slug = \Str::slug($center_name, '-');
        $slug_count_exercise= \App\Model\Exercise\Exercise::where('slug', $slug)->count();
        $slug_count_muscles =  \App\Model\Exercise\Muscles::where('slug', $slug)->count();
        $total_slug = (int) $slug_count_exercise + (int) $slug_count_muscles;
        if ($total_slug > 0) {
            return $slug . '-' . get5DigitRandom();
        } else {
            return $slug;
        }
    }

}
