<?php

namespace App\Http\Controllers\Admin\Exercise;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MusclesController extends MainController {

    public function __construct() {
        parent::__construct();
    }

    public function show() {
        return view('admin.exercise.manage_muscles');
    }

    public function musclesCategory() {
        return view('admin.exercise.manage_muscles_category');
    }

}
