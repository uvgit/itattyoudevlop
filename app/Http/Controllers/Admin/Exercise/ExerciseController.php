<?php

namespace App\Http\Controllers\Admin\Exercise;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Exercise\Muscles;
use Illuminate\Support\Facades\Input;
use App\Model\Exercise\CategoryMuscles;
use App\Model\Blog\Category;
use App\Model\Exercise\Exercise;

class ExerciseController extends MainController {

    public function __construct() {
        parent::__construct();
    }

    public function show() {
        return view('admin.exercise.dashboard');
    }

    public function view() {
        $CategoryMuscles = CategoryMuscles::where('status', 1)->get()->toArray();
        $Muscles = Muscles::where('status', 1)->get()->toArray();
        $Category = Category::get()->toArray();
        return view('admin.exercise.add_exercise', compact(['CategoryMuscles', 'Muscles', 'Category']));
    }

    public function addExercise() {
        $data = Input::all();

        $Exercise = Exercise::create([
                    "exercise_name" => $data['exercise_name'],
                    "other_name" => $data['other_name'],
                    "muscle_category" => (@checkArray($data['muscle_category'])) ? $data['muscle_category'] : [],
                    "main_muscle" => (@checkArray($data['main_muscle'])) ? $data['main_muscle'] : [],
                    "other_muscle" => (@checkArray($data['other_muscle'])) ? $data['other_muscle'] : [],
                    "equipments_needed" => $data['equipments_needed'],
                    "article_category" => (@checkArray($data['article_category'])) ? $data['article_category'] : [],
                    "add_video" => $data['add_video'],
                    "description" => htmlentities($data['description']),
                    "step1exercise" => htmlentities($data['step1exercise']),
                    "tipexercise" => htmlentities($data['tipexercise']),
                    "additional_exercise" => htmlentities($data['additional_exercise']),
                    "slug" => $this->slugByCounter($data['exercise_name']),
                    "status" => 1
        ]);

        prd($data);
    }

}
