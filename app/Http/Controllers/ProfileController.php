<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Session;
use Illuminate\Support\Facades\Input;
use App\User;
use DB;
use App\Artist;
use App\Studio;
use Illuminate\Database\Eloquent\Model;
class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Show the  profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $profileactive = "active";
        return view('profile.profile', compact('profileactive'));
    }
    /**
     * Show the user followers page.
     *
     * @return \Illuminate\Http\Response
     */
    public function Followers()
    {
        $followersactive    =   "active";
        $followers          =   User::GetUserFollowers();
        return view('profile.followers', compact('followersactive','followers'));
    }
    /**
     * Show the user Portfolio.
     *
     * @return \Illuminate\Http\Response
     */
    public function Portfolio()
    {
        $portfolioactive    =  "active";
        $userportfolio      =  User::GetUserPortfolio();
        $get_tatto_style    =  User::TottoStyle();
        $get_tatto_category =  User::GetTattoCategory();
        return view('profile.portfolio', compact('portfolioactive','get_tatto_category','get_tatto_style','userportfolio'));
    }
    /**
     * Show the user following page.
     *
     * @return \Illuminate\Http\Response
     */
    public function Following()
    {
        $followingactive    = "active";
        $following          = User::GetUserFollowing();
        return view('profile.following', compact('followingactive','following'));
    }
    /**
     * Show the user reviews.
     *
     * @return \Illuminate\Http\Response
     */
    public function Reviews()
    {
        $reviewsactive  =   "active";
        $reviews        =   User::GetUserReviews();
        return view('profile.reviews', compact('reviewsactive','reviews'));
    }
    /**
     * Change the user password
     *
     * @return \Illuminate\Http\Response
     */
    public function ChangePassword(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $password = $request->password;
            $confirm_password = $request->confirm_password;
            if((!empty($password) && !empty($confirm_password)) && ( $password ==  $confirm_password)) {
                $changedpassword = User::where('id', Auth::user()->id)->update(['password' => bcrypt($password)]);
                if($changedpassword){
                    Session::flash('success', "Password has been changed successfully");
                    return redirect()->back();
                }else {
                    Session::flash('error', "Some error occurred please try again later.");
                    return redirect()->back();
                }
            }else {
                Session::flash('error', "Password doesn't match.");
                return redirect()->back();
            }
        }else {
            Session::flash('error', "Some error occurred please try again later.");
            return redirect()->back();
        }
        
    }
     /**
     * Update user data
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateInfo(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $field = $request->field_update;
             if($field == 'email'){
                $check_email = DB::table('users')->where('id', '<>', Auth::user()->id)->where('email',$request->name)->count();
                if($check_email <= 0) {
                    $changefield = User::where('id', Auth::user()->id)->update(['email' => $request->name]);
                    return $request->name;
                }else {
                    return 1; // email already exist
                }
            }else {
                $changefield = User::where('id', Auth::user()->id)->update([$field => $request->name]);
                return $request->name;
            }
        }
        return 0; // Internal coding error
    }
     /**
     * Update user Logo
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateLogo(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $img        = $request->imageData;
            $img        = substr($img, strpos($img, ",") + 1);
            $file       = base64_decode($img);
            $image_name = Auth::user()->id.rand('0','5000').'_profile.png';
            $path       = public_path('images')."/user_profile_images/".$image_name;
            file_put_contents($path, $file);
            $get_pic = DB::table('users')->select('profile_pic')->where('id', Auth::user()->id)->first();
            if($get_pic->profile_pic != ''){
                $user_image_path = public_path('images')."/user_profile_images/".$get_pic->profile_pic;
                if(file_exists($user_image_path)){
                    unlink($user_image_path);
                }
            }
            $update_profile = User::where('id', Auth::user()->id)->update(['profile_pic' => $image_name]);
            return $image_name;
        }
        return 0; // Internal coding error
    }
     /**
     * Delete portfolio
     *
     * @return \Illuminate\Http\Response
     */
    public function DeletePortfolio(request $request){
       $method = $request->method();
        if ($request->isMethod('post')) {
            if($request->del_id != ''){
                 $get_pic = DB::table('portfolio')->select('image_name')->where('id',$request->del_id)->first();
                if($get_pic->image_name != ''){
                    $user_image_path = public_path('images')."/portfolio/".$get_pic->image_name;
                    if(file_exists($user_image_path)){
                        unlink($user_image_path);
                    }
                    DB::table('portfolio')->where('id', $request->del_id)->delete();
                    DB::table('user_tatto_category')->where('portfolio_id', $request->del_id)->delete();
                    Session::flash('success', "Portfolio deleted successfully");
                    return redirect()->back();
            }
            }else {
                Session::flash('error', "Some error occurred please try again later");
                return redirect()->back();
            }
        }else {
            Session::flash('error', "Some error occurred please try again later");
            return redirect()->back();
        }
         
    }
     /**
     * Add portfolio
     *
     * @return \Illuminate\Http\Response
     */
    public function AddProtfolio(request $request){
        $method = $request->method();
        $data = array();
        if ($request->isMethod('post')) {
            $img        = $request->tatto_image;
            if($img != '') {
                $arrImg    = explode('data:image/',$img);
                $final_img = explode(';', $arrImg[1]);
                $img        = substr($img, strpos($img, ",") + 1);
                $file       = base64_decode($img);
                $image_name = Auth::user()->id.rand('0','5000').'portfolio.'.$final_img[0];
                $path       = public_path('images')."/portfolio/".$image_name;
                file_put_contents($path, $file);
                if($request->tattoo_type <> '' &&  count($request->tattoo_category) > 0){
                    
                    $insert_portfolio = DB::table('portfolio')->insertGetId(
                                            ['user_id' => Auth::User()->id, 'image_name' => $image_name,'type'=>$request->tattoo_type]
                                        );
                    foreach($request->tattoo_category as $category){
                        $insert_category =  DB::table('user_tatto_category')->insert(
                                                ['user_id' => Auth::User()->id, 'tatto_category_id' => $category,'portfolio_id'=> $insert_portfolio]
                                            );
                    }
                    
                    if($insert_category) {
                           return 1;
                        
                    }else {
                        return  'Please try again some time..';
                    }
                }else {
                    return  'Some fields are required';
                }
            }else {
                return  'Image data not found';
            }
        }else {
            return 'Please try again some time..';
        }
       
         // Internal coding error
    }
     /**
     * Update user cover image
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateCover(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $img        = $request->imageData;
            $img        = substr($img, strpos($img, ",") + 1);
            $file       = base64_decode($img);
            $image_name = Auth::user()->id.rand('0','5000').'_profile.png';
            $path = public_path('images')."/user_cover_images/".$image_name;
            file_put_contents($path, $file);
            $get_pic = DB::table('users')->select('cover_pic')->where('id', Auth::user()->id)->first();
            if($get_pic->cover_pic != ''){
                $user_image_path = public_path('images')."/user_cover_images/".$get_pic->cover_pic;
                if(file_exists($user_image_path)){
                    unlink($user_image_path);
                }
            }
            $update_profile = User::where('id', Auth::user()->id)->update(['cover_pic' => $image_name]);
            return $image_name;
        }
        return 0; // Internal coding error
    }
    
    
}
