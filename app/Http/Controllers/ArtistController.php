<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Session;
use Illuminate\Support\Facades\Input;
use App\User;
use DB;
use App\Artist;
use Illuminate\Database\Eloquent\Model;
class ArtistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','artist'],['except' => ['home']]);
    }

    /**
     * Check validate.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'about' => 'required|max:1000',
            'experience' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'search_address' => 'required',
            'street_number' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'pincode' => 'required',
            'building_address' => 'required',
            'landmark' => 'required',
        ]);
    }

    /**
     * Show the application Home Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home');
    }
     /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

     /**
     * Show the artist about page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $aboutactive        = "active";
        $get_tatto          = User::TottoStyle();
        $get_artist         = Artist::GetArtistInfo();
        $get_tatto_style    = Artist::GetUserTattoStyle();
        return view('artist.about', compact('get_tatto','get_artist','get_tatto_style','aboutactive'));
    }
    /**
     * Change the user password
     *
     * @return \Illuminate\Http\Response
     */
    public function ChangePassword(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $password = $request->password;
            $confirm_password = $request->confirm_password;
            if((!empty($password) && !empty($confirm_password)) && ( $password ==  $confirm_password)) {
                $changedpassword = User::where('id', Auth::user()->id)->update(['password' => bcrypt($password)]);
                if($changedpassword){
                    Session::flash('success', "Password has been changed successfully");
                    return redirect()->back();
                }else {
                    Session::flash('error', "Some error occurred please try again later.");
                    return redirect()->back();
                }
            }else {
                Session::flash('error', "Password doesn't match.");
                return redirect()->back();
            }
        }else {
            Session::flash('error', "Some error occurred please try again later.");
            return redirect()->back();
        }
        
    }
     /**
     * Update user data
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateInfo(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $field = $request->field_update;
             if($field == 'email'){
                $check_email = DB::table('users')->where('id', '<>', Auth::user()->id)->where('email',$request->name)->count();
                if($check_email <= 0) {
                    $changefield = User::where('id', Auth::user()->id)->update(['email' => $request->name]);
                    return $request->name;
                }else {
                    return 1; // email already exist
                }
            }else {
                $changefield = User::where('id', Auth::user()->id)->update([$field => $request->name]);
                return $request->name;
            }
        }
        return 0; // Internal coding error
    }
     /**
     * Update user Logo
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateLogo(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $img = $request->imageData;
            $img = substr($img, strpos($img, ",") + 1);
            $file = base64_decode($img);
            $image_name= Auth::user()->id.rand('0','5000').'_profile.png';
            $path = public_path('images')."/user_profile_images/".$image_name;
            file_put_contents($path, $file);
            $get_pic = DB::table('users')->select('profile_pic')->where('id', Auth::user()->id)->first();
            if($get_pic->profile_pic != ''){
                $user_image_path = public_path('images')."/user_profile_images/".$get_pic->profile_pic;
                if(file_exists($user_image_path)){
                    unlink($user_image_path);
                }
            }
            $update_profile = User::where('id', Auth::user()->id)->update(['profile_pic' => $image_name]);
            return $image_name;
        }
        return 0; // Internal coding error
    }
     /**
     * Update user cover image
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateCover(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $img = $request->imageData;
            $img = substr($img, strpos($img, ",") + 1);
            $file = base64_decode($img);
            $image_name= Auth::user()->id.rand('0','5000').'_profile.png';
            $path = public_path('images')."/user_cover_images/".$image_name;
            file_put_contents($path, $file);
            $get_pic = DB::table('users')->select('cover_pic')->where('id', Auth::user()->id)->first();
            if($get_pic->cover_pic != ''){
                $user_image_path = public_path('images')."/user_cover_images/".$get_pic->cover_pic;
                if(file_exists($user_image_path)){
                    unlink($user_image_path);
                }
            }
            $update_profile = User::where('id', Auth::user()->id)->update(['cover_pic' => $image_name]);
            return $image_name;
        }
        return 0; // Internal coding error
    }
      /**
     * Save Artist Data
     *
     * @return \Illuminate\Http\Response
     */
    public function SaveArtist(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            list($lat,$long) = explode("|", $request->latlong);
            $get_artist = Artist::GetArtistInfo();
            if(count($get_artist) > 0){
                $artist                 = Artist::find($get_artist->id);   
            }else {
                $artist                 = new Artist();
            }
            $artist->about              = $request->about;
            $artist->experience         = $request->experience;
            $artist->gender             = $request->gender;
            $artist->birthday           = $request->birthday;
            $artist->search_address     = $request->search_address;
            $artist->route              = $request->route;
            $artist->street_number      = $request->street_number;
            $artist->city               = $request->city;
            $artist->state              = $request->state;
            $artist->country            = $request->country;
            $artist->pincode            = $request->postal_code;
            $artist->lat                = $lat;
            $artist->long               = $long;
            $artist->building_address   = $request->building_address;
            $artist->landmark           = $request->landmark;
            $artist->charge_per_hour    = $request->charge_per_hour;
            $artist->charge_per_inch    = $request->charge_per_inch;
            $artist->website            = $request->website;
            $artist->user_id            = Auth::user()->id;
            $status                     = $artist->save();
            if(count($request->tatto_style) > 0){
                if(count($get_artist) > 0){
                    DB::table('user_tatto_style')->where('user_id',Auth::user()->id)->delete();
                }
                foreach($request->tatto_style as $tatto) {
                    DB::table('user_tatto_style')->insert(
                        ['user_id' => Auth::user()->id, 'tatto_style_id' => $tatto]
                    );
                }
            }
            if($status){
                if(count($get_artist) > 0){
                    Session::flash('success', "Information Updated Successfully");
                }else {
                    Session::flash('success', "Information Added Successfully");
                }
                return redirect()->back(); 
            }else {
                Session::flash('error', "Some error occurred please try again later.");
                return redirect()->back();
            }
        }else {
            Session::flash('error', "Some error occurred please try again later.");
            return redirect()->back();
        }
    }
}
