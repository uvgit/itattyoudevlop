<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Session;
use Illuminate\Support\Facades\Input;
use App\User;
use DB;
use App\Studio;
use Illuminate\Database\Eloquent\Model;
class StudioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','studio'],['except' => ['home']]);
    }

    /**
     * Check validate.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'about' => 'required|max:1000',
            'search_address' => 'required',
            'street_number' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'pincode' => 'required',
            'building_address' => 'required',
            'landmark' => 'required',
        ]);
    }

  
     /**
     * Show the studio about page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $aboutactive                = "active";
        $get_studio_facilities      = Studio::StudioFacilities();
        $get_studio                 = Studio::GetStudioInfo();
        $get_user_studio_facilities = Studio::GetUserStudioFacilities();
        return view('studio.about', compact('get_studio_facilities','get_studio','get_user_studio_facilities','aboutactive'));
    }
    public function SaveStudio(request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            list($lat,$long) = explode("|", $request->latlong);
            $get_studio = Studio::GetStudioInfo();
            if(count($get_studio) > 0){
                $studio                 = Studio::find($get_studio->id);   
            }else {
                $studio                 = new Studio();
            }
            $studio->about              = $request->about;
            $studio->establishment_date = $request->establishment_date;
            $studio->search_address     = $request->search_address;
            $studio->route              = $request->route;
            $studio->street_number      = $request->street_number;
            $studio->city               = $request->city;
            $studio->state              = $request->state;
            $studio->country            = $request->country;
            $studio->pincode            = $request->postal_code;
            $studio->lat                = $lat;
            $studio->long               = $long;
            $studio->building_address   = $request->building_address;
            $studio->landmark           = $request->landmark;
            $studio->user_id            = Auth::user()->id;
            $status                     = $studio->save();
            if(count($request->studio_facilities) > 0){
                if(count($get_studio) > 0){
                    DB::table('user_studio_facilities')->where('user_id',Auth::user()->id)->delete();
                }
                foreach($request->studio_facilities as $facilities) {
                    DB::table('user_studio_facilities')->insert(
                        ['user_id' => Auth::user()->id, 'studio_facility_id' => $facilities]
                    );
                }
            }
            if($status){
                if(count($get_studio) > 0){
                    Session::flash('success', "Information Updated Successfully");
                }else {
                    Session::flash('success', "Information Added Successfully");
                }
                return redirect()->back(); 
            }else {
                Session::flash('error', "Some error occurred please try again later.");
                return redirect()->back();
            }
        }else {
            Session::flash('error', "Some error occurred please try again later.");
            return redirect()->back();
        }
    }
}
