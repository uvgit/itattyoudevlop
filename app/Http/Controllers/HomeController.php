<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;
use App\Studio;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin'],['except' => ['about','home','Search']]);
    }

    /**
     * Show the application Home Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home');
    }
    /**
     * Show the Search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function Search($city = null, $street = null, $cat = null, $page = null, $limit = null)
    {
        $city = str_replace("-", " ", $city);
        $street = str_replace("-", " ", $street);
        if($street == 'artist'){
            $street = '';
        }
        $artistdata= DB::table('artist as a')
                ->select(['a.*','b.*',DB::raw('round(avg(c.star),2) as rating'),DB::raw('count(c.star) as rating_count')])
                ->leftjoin('users as b' , 'b.id', '=', 'a.user_id')
                ->leftjoin('reviews as c' , 'b.id', '=', 'c.review_to')
                ->where('a.city', 'like', "%$city%")
                ->where('a.street_number', 'like', "%$street%")
                ->groupBy('a.id')
                ->get();
        $studiodata=DB::table('studio as a')
                ->select(['a.*','b.*',DB::raw('round(avg(c.star),2) as rating'),DB::raw('count(*) as rating_count')])
                ->leftjoin('users as b' , 'b.id', '=', 'a.user_id')
                ->leftjoin('reviews as c' , 'b.id', '=', 'c.review_to')
                ->where('a.city', 'like', "%$city%")
                ->where('a.street_number', 'like', "%$street%")
                ->groupBy('a.id')
                ->get();
        return view('search.index',compact('street','city','artistdata','studiodata'));
    }
     /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }
     /**
     * Show the About Us Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('about');
    }
}
