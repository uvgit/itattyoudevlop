<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use DB;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','last_name','role_type','contact_no'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * Get user followers
     *
     * @return \Illuminate\Http\Response
     */
    public static function GetUserFollowers(){
        return DB::table('followers')
                ->leftJoin('users', 'users.id', '=', 'followers.follow_by')
                ->where('followers.follow_to',Auth::user()->id)->get();
    }
     /**
     * Get user following
     */
    public static function GetUserFollowing(){
        return DB::table('followers')
                ->leftJoin('users', 'users.id', '=', 'followers.follow_to')
                ->where('followers.follow_by',Auth::user()->id)->get();
    }
     /**
     * Get user portfolio
     */
    public static function GetUserPortfolio(){
        return DB::table('portfolio')
                ->where('user_id',Auth::user()->id)
                ->where('status',0)
                ->get();
    }
     /**
     * Get user portfolio tatto category
     */
    public static function GetTattoCategory(){
        return DB::table('tatto_category')
                ->where('status',0)->get();
    }
    
    /**
     * Get Tatto Style.
     *
     * @return \Illuminate\Http\Response
     */
    public static function TottoStyle(){
        return DB::table('tatto_style')->where('status',0)->get();
    }
    /**
     * Get user reviews
    */
    public static function GetUserReviews(){
        return DB::table('reviews as r')
                ->select(['u.name','u.last_name','r.review','r.created_at','r.star'])
                ->leftJoin('users as u', 'u.id', '=', 'r.review_by')
                ->where('r.review_to',Auth::user()->id)->get();
    }
    /**
     * Get review 
     */
    public static function GetDate($date){
        $time = strtotime($date);
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }
    }
}
