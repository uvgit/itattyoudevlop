<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class Artist extends Model
{
    protected $table = 'artist';
    protected $fillable = [
        'user_id',
        'about',
        'experience',
        'gender',
        'birthday',
        'search_address',
        'route',
        'street_number',
        'city',
        'state',
        'country',
        'pincode',
        'lat',
        'long',
        'building_address',
        'landmark',
        'charge_per_hour',
        'charge_per_inch',
        'website',
        'status'
    ];
    /**
     * Get User Tatto Style.
     *
     * @return \Illuminate\Http\Response
     */
    public static function GetUserTattoStyle(){
        $usertattostyle = array();
        $data =  DB::table('user_tatto_style')->select('tatto_style_id')->where("user_id",Auth::user()->id)->get();
        foreach($data as $userdata){
            $usertattostyle[] =  $userdata->tatto_style_id;
        }
        return $usertattostyle;
    }
    /**
     * Get Artist info.
     *
     * @return \Illuminate\Http\Response
     */
    public static function GetArtistInfo(){
        return DB::table('artist')->where('status',0)->where('user_id', Auth::user()->id)->first();
    }
}
