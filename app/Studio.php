<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class Studio extends Model
{
    protected $table = 'studio';
    protected $fillable = [
        'user_id',
        'about',
        'establishment_date',
        'search_address',
        'route',
        'street_number',
        'city',
        'state',
        'country',
        'pincode',
        'lat',
        'long',
        'building_address',
        'landmark',
        'status'
    ];
    /**
     * Get Studio Facilities.
     *
     * @return \Illuminate\Http\Response
     */
    public static function StudioFacilities(){
        return DB::table('studio_facilities')->where('status',0)->get();
    }
    /**
     * Get User Studio facilities.
     *
     * @return \Illuminate\Http\Response
     */
    public static function GetUserStudioFacilities(){
        $userstudiofacilities = array();
        $data =  DB::table('user_studio_facilities')->select('studio_facility_id')->where("user_id",Auth::user()->id)->get();
        foreach($data as $userdata){
            $userstudiofacilities[] =  $userdata->studio_facility_id;
        }
        return $userstudiofacilities;
    }
    /**
     * Get Studio info.
     *
     * @return \Illuminate\Http\Response
     */
    public static function GetStudioInfo(){
        return DB::table('studio')->where('status',0)->where('user_id', Auth::user()->id)->first();
    }
}
