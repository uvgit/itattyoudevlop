/* 
 *  Copyright 2016 p3multisolutions Pvt Ltd
 *  Author : Kanhaiya Sharma <sharma00ji@gmail.com>
 *  Licensed under the p3multisolutions Pvt Ltd License, Version 1.0   * 
 */
$(function () {
    var Register = function () {
        var self = this;
        var url;
        self.init = function (traget) {
            var traget = $(traget);
            var token = $('meta[name="csrf-token"]').attr('content');
            self.url = traget.data('url');
            console.log(traget);
            self.login();

        },
                self.registerNow = function () {

                    $('.regisernow').fancybox({
                        maxWidth: 489,
                        fitToView: false,
                        width: '70%',
                        height: "100%",
                        autoSize: true,
                        closeClick: false,
                        openEffect: 'none',
                        closeEffect: 'none'
                    });
                    $.ajax({
                        url: self.url,
                        type: "GET",
                        success: function (data) {

                            $('#popup_login').html(data);

                            $('#popup_login').show();
                        }
                    });
                    $('#popup_login').show();

                },
                self.login = function () {
                    $(".user_account").fancybox({
                        maxWidth: 489,
                        fitToView: false,
                        width: '70%',
                        height: "100%",
                        autoSize: true,
                        closeClick: false,
                        openEffect: 'none',
                        closeEffect: 'none'
                    });
                    $.ajax({
                        url: self.url,
                        type: "GET",
                        success: function (data) {

                            $('#popup_login').html(data);
                            $('#popup_login').show();
                        }
                    });
                    $('#popup_login').show();
                },
                self.Registersubmit = function (param) {
                    var formdata = $(param).serializeArray();
                    var token = $(param).find('input[name="_token"]').val();
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': token
                        },
                        url: self.url,
                        type: "post",
                        data: formdata,
                        success: function (data) {
                            $('.error').html('');
                            console.log(data)
                        }, error: function (data) {
                            $(param).find("span").each(function (k, v) {
                                $(this).text('');
                            });
                            responseText = $.parseJSON(data.responseText);

                            $.each(responseText, function (i, j) {
                                $('#' + i).text(j);
                            })

                        }
                    });
                    console.log(formdata)

                }

    }

    var Register = new Register()
    $('body').delegate('a', 'click', function () {
        var url = $(this).data('url');
        if (url) {
            Register.init(this);

        }
    });
    $(document).on('submit', '#registerUsers', function (e) {
        Register.Registersubmit(this);
        return false;
    });

})

