
    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        //return pass;
        return "admin@123";
    }
    
    $('.genratePass').click(function(){
        password = randomPassword(12);
        $('.generatePassword').val(password);
    })
    
    $(document).ready(function() {
        $("#longDesc").on('keyup', function() { 
            if($(this).val().length){
                var words = this.value.match(/\S+/g).length;
                if (words > 200) {
                    // Split the string on first 200 words and rejoin on spaces
                    var trimmed = $(this).val().split(/\s+/, 200).join(" ");
                    // Add a space at the end to keep new typing making new words
                    $(this).val(trimmed + " ");
                }
                else {
                    $('#display_count').text(words);
                    $('#word_left').text(200-words);
                }
            }
            else{
                $('#word_left').text(200);
            }
        });
        

        $('#button').click(function () {
            $("input[type='file']").trigger('click');
        })

        $("input[type='file']").change(function () {
            $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
        })
     }); 
    
    $(window).load(function() {
        $("#longDesc").trigger('keyup'); 
    })
