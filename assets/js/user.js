    
    $('.deleteUser').click(function(){
        confirmation = confirm('Are you sure to delete this user!');
        if(confirmation){
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('.dropdown').removeClass('open');
        }            
    })
    $('.resetPassword').click(function(){
        confirmation = confirm('Are you sure to reset password for this user!');
        if(confirmation){
            $(this).closest('form').submit();
        }
        else{
            $(this).closest('.dropdown').removeClass('open');
        }            
    });

    $('#multiUserRoleBtm').click(function(){
        var selected = new Array();
        $("input:checkbox[name='filter-check[]']:checked").each(function() {
           selected.push($(this).val());
        });
        if(selected.length){
           // alert(SITE_URL);
            roleId = $('#role').val();
            if(roleId){
                
            $.ajax({
                url:SITE_URL+'/admin/assignRoleToMultiUser',
                type:'post', 
                data:{'_token':$('#_token').val(),roleId:roleId,users:selected},
                success:function(res){
                    if(res){
                        $('#role').val('');
                        $('#role').select2('');
                        $("input:checkbox[name='filter-check[]']").removeAttr('checked');
                        window.location= SITE_URL+'/admin/access-control';
                    }
                    else{
                        alert('Some Error Occured');
                    }
                    
                },
                error:function(err){
                    alert('Some Error Occured');
                }
            })
            }else{
                alert("Select Role!");
            }
        }else{
            alert("Select atleast one user!")
        }
    });


    function checkAll(e) {
        var checkboxes = document.getElementsByTagName('input');
        if (e.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        }else {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
                }
            }
        }
    }
