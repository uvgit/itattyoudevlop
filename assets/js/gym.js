function validateDynamicElement() {
    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
        }
    });
}

/***** Javascript for Gyms starts ****/
$(function () {
    $('.datetimepicker').datetimepicker({
        maskInput: true, // disables the text input mask
        pickDate: false, // disables the date picker
        pickTime: true, // disables de time picker
        pick12HourFormat: true, // enables the 12-hour format time picker
        pickSeconds: false, // disables seconds in the time picker
        //startDate: -Infinity,      // set a minimum date
        //endDate: Infinity          // set a maximum date
    });
});
var timing = $('#gymTiming').val();
if (timing != "") {
    if (timing == "custom") {
        $('#default_div').hide('slow');
        $('#default_div').find('input').attr('disabled', 'disabled');
        $('#custom_div').show('slow');
        $('#custom_div').find('input').removeAttr('disabled');
    }
    if (timing == "default") {
        $('#default_div').show('slow');
        $('#default_div').find('input').removeAttr('disabled');
        $('#custom_div').hide('slow');
        $('#custom_div').find('input').attr('disabled', 'disabled');
    }
}
$('#gymTiming').change(function () {
    timing = $(this).val();
    console.log(timing);
    if (timing != "") {
        if (timing == "custom") {
            $('#default_div').hide('slow');
            $('#default_div').find('input').attr('disabled', 'disabled');
            $('#custom_div').show('slow');
            $('#custom_div').find('input').removeAttr('disabled');
        }
        if (timing == "default") {
            $('#default_div').show('slow');
            $('#default_div').find('input').removeAttr('disabled');
            $('#custom_div').hide('slow');
            $('#custom_div').find('input').attr('disabled', 'disabled');
        }
    }
})
$('#create_gym').click(function () {
    $(this).closest('form').submit();

})
/***** Javascript for Gyms ends ****/

/*
 $('#copyAllWeekday').click(function () {
 morningFrom = $(this).closest('#monday').find('.morningFrom').val();
 morningTo = $(this).closest('#monday').find('.morningTo').val();
 eveningFrom = $(this).closest('#monday').find('.eveningFrom').val();
 eveningTo = $(this).closest('#monday').find('.eveningTo').val();
 $('#timingSchedule').find('.morningFrom').val(morningFrom);
 $('#timingSchedule').find('.morningTo').val(morningTo);
 $('#timingSchedule').find('.eveningFrom').val(eveningFrom);
 $('#timingSchedule').find('.eveningTo').val(eveningTo);
 })
 $('#copyAllWeekend').click(function () {
 morningFrom = $(this).closest('#saturday').find('.morningFrom').val();
 morningTo = $(this).closest('#saturday').find('.morningTo').val();
 eveningFrom = $(this).closest('#saturday').find('.eveningFrom').val();
 eveningTo = $(this).closest('#saturday').find('.eveningTo').val();
 $('#timingSchedule').find('.weekend').filter('.morningFrom').val(morningFrom);
 $('#timingSchedule').find('.weekend').filter('.morningTo').val(morningTo);
 $('#timingSchedule').find('.weekend').filter('.eveningFrom').val(eveningFrom);
 $('#timingSchedule').find('.weekend').filter('.eveningTo').val(eveningTo);
 })*/
$('body').delegate('.member_plan', 'change', function () {
    if ($(this).val() == 'Other') {
        row = parseInt($(this).attr('data-row'));
        $(this).parent('div').parent('div').removeClass('col-md-6 col-md-2').addClass('col-md-2');
        other_plan_div = '<div class="other_plan_name"><div class="col-md-2 other_plan_name"><div class="form-group"><label>Package Name</label>' +
                '<input value="" type="text" name="package[' + row + '][other_name]" class="form-control" placeholder="Package Name">' +
                '</div></div>';
        other_plan_div += '<div class="col-md-2 other_plan_name"><div class="form-group"><label>Duration</label>' +
                '<input type="text" name="package[' + row + '][duration]" class="form-control" placeholder="30">' +
                '</div></div></div>';
//        other_plan_div += '<div class="col-md-1"><div class="form-group"><label>Validity</label>' +
//                        '<input type="text" name="package[' + row + '][validity]" class="form-control" placeholder="30">' +
//                        '</div></div></div>';                                
        $(this).parent('div').parent('div').after(other_plan_div);
    }
    else {
        $(this).parent('div').parent('div').next('.other_plan_name').remove();
        $(this).parent('div').parent('div').removeClass('col-md-2 col-md-6').addClass('col-md-6');
    }
})

/*$('body').delegate('.addMorePhotoBtn', 'click', function () {
 photoRow = parseInt($('#photoRow').val());
 photoRow = photoRow + 1;
 $('#photoRow').val(photoRow);
 str = '<div class="row planDiv">' +
 '<div class="col-md-9"><div class="form-group"><label>Add Photo</label>'+
 '<input type="text" name="photos['+photoRow+']" readonly id="photo['+photoRow+']"  class="form-control">'+
 '</div></div>' +
 '<div class="col-md-1"><div class="form-group"><label>&nbsp;</label>'+
 '<button type="button" class="btn btn-info" onclick=BrowseServer("photo['+photoRow+']");><i class="fa fa-camera"></i> Pick Image</button>'+
 '</div></div>' +
 '<div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control addMorePhotoBtn" value="Add More" id="">' +
 '</div></div><div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control deletePlanBtn" value="Delete" id=""></div></div></div>';
 $('#addMorePhoto').append(str);
 })*/

$('body').delegate('.addMorePlanBtn', 'click', function () {
    pacakgeRow = parseInt($('#pacakgeRow').val());
    pacakgeRow = pacakgeRow + 1;
    $('#pacakgeRow').val(pacakgeRow);
    str = '<div class="row planDiv">' +
            '<div class="col-md-6"><div class="form-group"><label>Package Name</label>' +
            '<select class="select member_plan" name="package[' + pacakgeRow + '][packageName]" data-row="' + pacakgeRow + '"><option value="">Select Package</option>' +
            '<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option>' +
            '<option value="Half yearly">Half yearly</option><option value="Annually">Annually</option>' +
            '<option value="Other">Other</option></select>' +
            '</div></div>' +
            '<div class="col-md-2"><div class="form-group"><label>MRP Price</label>' +
            '<input type="text" name="package[' + pacakgeRow + '][packagePrice]" class="form-control" placeholder="2000">' +
            '</div></div><div class="col-md-2"><div class="form-group"><label>Discount</label>' +
            '<input type="text" name="package[' + pacakgeRow + '][packageDisPrice]" class="form-control" placeholder="">' +
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control addMorePlanBtn" value="Add More" id="">' +
            '</div></div><div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control deletePlanBtn" value="Delete" id=""></div></div></div>';
    $('#addMorePlan').append(str);
    $('.select').select2();
    validateDynamicElement();
})

$('body').delegate('.deletePlanBtn', 'click', function () {
    $(this).closest('.planDiv').remove();
});

$('body').delegate('.addMobileBtn', 'click', function () {
    mobileRow = parseInt($('#mobileRow').val());
    mobileRow = mobileRow + 1;
    $('#mobileRow').val(mobileRow);
    str = '<div class="planDiv">' +
            ' <div class="ip_fld_m mrbt_10 add_defaut_num">' +
            ' <div class="default_num_phn">+91</div>' +
            ' <input type="hidden" name="mobile[mobile_code][]" value="+91"  />' +
            ' <input ' +
            ' data-validation-optional="true"' +
            ' data-validation="required number length"' +
            ' data-validation-length="10"' +
            ' data-validation-error-msg-number="Enter valid number"' +
            'data-validation-error-msg-required="Required"' +
            ' data-validation-error-msg-length="Your mobile number must be 10 digits"' +
            ' name="mobile[number][]" class="form-control valid" value="" placeholder="Phone" type="text"><span class="mob_delete">-</span>' +
            '</div></div>';

    $('.addMobileDiv').append(str);
    validateDynamicElement();
})

$('body').delegate('.addLandlineBtn', 'click', function () {
    landlineRow = parseInt($('#landlineRow').val());
    landlineRow = landlineRow + 1;
    $('#landlineRow').val(landlineRow);
    str = '<div class="planDiv">' +
            '<div class="ip_fld_m mrbt_10 add_defau_land">' +
            '<div class="default_num_phn">+91</div>' +
            '<div class="default_num_land">' +
            '<input type="hidden" name="landline[landline_code][]" value="+91"  />' +
            '<input type="text" ' +
            'data-validation-optional="true"' +
            'data-validation="number length"' +
            'data-validation-length="2-5"' +
            'data-validation-error-msg-length="Extension must be between 2-5 digits"' +
            'data-validation-error-msg-number="Enter valid extension"' +
            'name="landline[landline_ext][]" value="" placeholder="STD Code" />' +
            '</div>' +
            ' <input ' +
            ' data-validation-optional="true"' +
            ' data-validation="number length"' +
            ' data-validation-length="5-9"' +
            ' data-validation-error-msg-number="Enter valid number"' +
            ' data-validation-error-msg-length="Your Landline number must be between 5-9 digits"' +
            ' name="landline[number][]" class="form-control valid ip_lan_num" value="" placeholder="Landline" type="text"><span class="lan_delete">-</span>' +
            '</div></div>';

    $('.addLandlineDiv').append(str);
    validateDynamicElement();
})

$('body').delegate('.mob_delete,.lan_delete', 'click', function (e) {

    $(this).closest('.planDiv').remove();

});

$('.closed_status_m').change(function () {
    if ($(this).prop("checked") == true) {
        $(this).closest('.timeDiv').find('.timeDiv_m').attr('disabled', 'disabled');
    } else {
        $(this).closest('.timeDiv').find('.timeDiv_m').removeAttr('disabled');
    }
});
$('.closed_status_e').change(function () {
    if ($(this).prop("checked") == true) {
        $(this).closest('.timeDiv').find('.timeDiv_e').attr('disabled', 'disabled');
    } else {
        $(this).closest('.timeDiv').find('.timeDiv_e').removeAttr('disabled');
    }
});
$('.closed_status_e').trigger('change');
$('.closed_status_m').trigger('change');

$('.time_pik').on('keydown', function () {
    return false;
})
$('body').delegate('.deletelogcover', 'click', function (e) {
    var confirmation = confirm("Are you sure you want to delete?");
    if (confirmation) {
        var action = $(this).data('action')
        var user = $(this).data('user')
        var user_id = $(this).data('user_id')
        if (action == 'cover') {
            $('.deleteLoadercover').css("display", "block");
        }
        if (action == 'logo') {
            $('.deleteLoaderlogo').css("display", "block");
        }

        if (action) {
            $.ajax({
                url: SITE_URL + '/api/1.0/gym/' + action + '/delete',
                data: {user: user, user_id: user_id, _token: $('#_token').val()},
                type: 'post',
                success: function (data) {
                    if (data.error == false) {
                        $(this).remove();
                        if (action == 'cover') {
                            $('#uploaded_cover').attr('src', SITE_URL + '/assets/images/photo-gallery.jpg');
                            $('#uploaded_cover_input').val('');
                        }
                        if (action == 'logo') {
                            $('#uploaded_logo').attr('src', SITE_URL + '/assets/images/photo-gallery.jpg');
                            $('#uploaded_logo_input').val('');
                        }
                    }
                    if (action == 'cover') {
                        $('.deleteLoadercover').css("display", "none");
                    }
                    if (action == 'logo') {
                        $('.deleteLoaderlogo').css("display", "none");
                    }
                },
                error: function (err) {
                    alert('Try After Sometime...');
                    if (action == 'cover') {
                        $('.deleteLoadercover').css("display", "none");
                    }
                    if (action == 'logo') {
                        $('.deleteLoaderlogo').css("display", "none");
                    }
                }
            })
        }
    }
});
