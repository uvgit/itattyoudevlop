if($('#latlong').length){
    if(!$('#latlong').val().trim().length){
        $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').attr('readonly', true).addClass('autodata disable');
    }
}
$('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').click(function(){
    if ($(this).hasClass('autodata')) {
        alert('Search your address to fill this');
    } 
});
$('#autocomplete').keyup(function(){
    if(!$(this).val().trim().length){
        $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').val('').attr('readonly', true).addClass('autodata disable');
    }
})
if($('.latlong').length){
    if(!$('.latlong').val().trim().length){
        $('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').attr('readonly', true).addClass('autodata disable');
    }
}
$('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').click(function(){
    if ($(this).hasClass('autodata')) {
        alert('Search your address to fill this');
    } 
});
$('#autocomplete_2').keyup(function(){
    if(!$(this).val().trim().length){
        $('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').val('').attr('readonly', true).addClass('autodata disable');
    }
})
var placeSearch, autocomplete,autocomplete_2;
function addressAutocomplete() { 
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var options = {
          types: ['geocode'],
          componentRestrictions: {country: "in"}
         };
    autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            options);
            
    autocomplete.addListener('place_changed', fillInAddress);
    addressAutocomplete_2();
}

function addressAutocomplete_2() { 
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var options = {
          types: ['geocode'],
          componentRestrictions: {country: "in"}
         };
    autocomplete_2 = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete_2')),
            options);
    autocomplete_2.addListener('place_changed', fillInAddress_2);
}

function fillInAddress() { 
    $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').val('');
    $('#street_number, #route,#city,#administrative_area_level_1,#country,#postal_code').removeClass('autodata');
    $('#route,#city,#administrative_area_level_1,#country,#postal_code').attr('readonly', true).addClass('disable');
    //$('#route,#city,#administrative_area_level_1,#country,#postal_code').removeAttr('readonly').removeClass('disable');
    //$('#street_number').removeClass('disable');
    var componentForm = {
        route: '',
        sublocality_level_3: '',
        sublocality_level_2: '',
        sublocality_level_1: '',
        locality: '',
        administrative_area_level_1: '',
        country: '',
        postal_code: ''
    };
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    
    //console.log(place.address_components);
    $.each(place.address_components, function (index, value) {
        componentForm[value.types[0]] = value.long_name;
    });
    //console.log(componentForm);

    if (componentForm.route != "" && componentForm.sublocality_level_3 == "") {
        route = componentForm.route;
    } else if (componentForm.route == "" && componentForm.sublocality_level_3 != "") {
        route = componentForm.sublocality_level_3;
    } else if (componentForm.route != "" && componentForm.sublocality_level_3 != "") {
        route = componentForm.route + ', ' + componentForm.sublocality_level_3;
    } else {
        route = "";
    }

    if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 == "") {
        street_number = componentForm.sublocality_level_1;
    } else if (componentForm.sublocality_level_1 == "" && componentForm.sublocality_level_2 != "") {
        street_number = componentForm.sublocality_level_2;
    } else if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 != "") {
        street_number = componentForm.sublocality_level_1 + ', ' + componentForm.sublocality_level_2;
    } else {
        street_number = "";
    }
    if(street_number.trim().length)
        $('#street_number').val(street_number);
    else
        $('#street_number').val(route);
    $('#route').val(route);
    $('#city').val(componentForm.locality);
    $('#administrative_area_level_1').val(componentForm.administrative_area_level_1);
    $('#country').val(componentForm.country);
    $('#postal_code').val(componentForm.postal_code);
    
    
    if(!route.trim().length)
        $('#route').removeAttr('readonly').removeClass('disable');
    if(!componentForm.locality.trim().length)
        $('#city').removeAttr('readonly').removeClass('disable');
    if(!componentForm.administrative_area_level_1.trim().length)
        $('#administrative_area_level_1').removeAttr('readonly').removeClass('disable');
    if(!componentForm.country.trim().length)
        $('#country').removeAttr('readonly').removeClass('disable');
    if(!componentForm.postal_code.trim().length)
        $('#postal_code').removeAttr('readonly').removeClass('disable');
    
    latlong = place.geometry.location.lat()+'|'+place.geometry.location.lng();    
    $('#latlong').val(latlong);
    $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').trigger('keyup');
    $('#search_submit').trigger('click');
    
}

function fillInAddress_2() { 
    $('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').val('');
    $('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').removeClass('autodata');
    $('.route,.city,.administrative_area_level_1,.country,.postal_code').attr('readonly', true).addClass('disable');
    //$('.route,.city,.administrative_area_level_1,.country,.postal_code').removeAttr('readonly').removeClass('disable');
    //$('.street_number').removeClass('disable');
    var componentForm = {
        route: '',
        sublocality_level_3: '',
        sublocality_level_2: '',
        sublocality_level_1: '',
        locality: '',
        administrative_area_level_1: '',
        country: '',
        postal_code: ''
    };
    // Get the place details from the autocomplete object.
    var place = autocomplete_2.getPlace();
    //console.log(place);

    $.each(place.address_components, function (index, value) {
        componentForm[value.types[0]] = value.long_name;
    });
    //console.log(componentForm);

    if (componentForm.route != "" && componentForm.sublocality_level_3 == "") {
        route = componentForm.route;
    } else if (componentForm.route == "" && componentForm.sublocality_level_3 != "") {
        route = componentForm.sublocality_level_3;
    } else if (componentForm.route != "" && componentForm.sublocality_level_3 != "") {
        route = componentForm.route + ', ' + componentForm.sublocality_level_3;
    } else {
        route = "";
    }

    if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 == "") {
        street_number = componentForm.sublocality_level_1;
    } else if (componentForm.sublocality_level_1 == "" && componentForm.sublocality_level_2 != "") {
        street_number = componentForm.sublocality_level_2;
    } else if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 != "") {
        street_number = componentForm.sublocality_level_1 + ', ' + componentForm.sublocality_level_2;
    } else {
        street_number = "";
    }
    if(street_number.trim().length)
        $('.street_number').val(street_number);
    else
        $('.street_number').val(route);
    $('.route').val(route);
    $('.city').val(componentForm.locality);
    $('.administrative_area_level_1').val(componentForm.administrative_area_level_1);
    $('.country').val(componentForm.country);
    $('.postal_code').val(componentForm.postal_code);
    
    if(!route.trim().length)
        $('.route').removeAttr('readonly').removeClass('disable');
    if(!componentForm.locality.trim().length)
        $('.city').removeAttr('readonly').removeClass('disable');
    if(!componentForm.administrative_area_level_1.trim().length)
        $('.administrative_area_level_1').removeAttr('readonly').removeClass('disable');
    if(!componentForm.country.trim().length)
        $('.country').removeAttr('readonly').removeClass('disable');
    if(!componentForm.postal_code.trim().length)
        $('.postal_code').removeAttr('readonly').removeClass('disable');
    
    latlong = place.geometry.location.lat()+'|'+place.geometry.location.lng();    
    $('.latlong').val(latlong);
    $('.street_number,.route,.city,.administrative_area_level_1,.country,.postal_code').trigger('keyup');
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
            autocomplete_2.setBounds(circle.getBounds());
        });
    }
}



    var placeSearch, autocomplete;
    function initAutocomplete() { 
       
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('autocomplete'));

        var types = document.getElementById('type-selector');
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {     
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            
            /**** Insert into serch boxes value starts ******/
            var componentForm = {
                route: '',
                sublocality_level_3: '',
                sublocality_level_2: '',
                sublocality_level_1: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: ''
            };
            
            $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').val('');
            $('#street_number,#route,#city,#administrative_area_level_1,#country,#postal_code').removeAttr('disabled');
            $.each(place.address_components, function (index, value) {
                componentForm[value.types[0]] = value.long_name;
            });
            //console.log(componentForm);

            if (componentForm.route != "" && componentForm.sublocality_level_3 == "") {
                route = componentForm.route;
            } else if (componentForm.route == "" && componentForm.sublocality_level_3 != "") {
                route = componentForm.sublocality_level_3;
            } else if (componentForm.route != "" && componentForm.sublocality_level_3 != "") {
                route = componentForm.route + ', ' + componentForm.sublocality_level_3;
            } else {
                route = "";
            }

            if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 == "") {
                street_number = componentForm.sublocality_level_1;
            } else if (componentForm.sublocality_level_1 == "" && componentForm.sublocality_level_2 != "") {
                street_number = componentForm.sublocality_level_2;
            } else if (componentForm.sublocality_level_1 != "" && componentForm.sublocality_level_2 != "") {
                street_number = componentForm.sublocality_level_1 + ', ' + componentForm.sublocality_level_2;
            } else {
                street_number = "";
            }
            $('#street_number').val(street_number);
            $('#route').val(route);
            $('#city').val(componentForm.locality);
            $('#administrative_area_level_1').val(componentForm.administrative_area_level_1);
            $('#country').val(componentForm.country);
            $('#postal_code').val(componentForm.postal_code);

            latlong = place.geometry.location.lat()+'|'+place.geometry.location.lng();    
            $('#latlong').val(latlong);
            
            /**** Insert into serch boxes value ends ******/
          
          
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
        });
    }
  
    function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
          });
        }
    }
