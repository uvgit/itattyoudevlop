var urlobj;
function BrowseServer(obj)
{
    urlobj = obj;
    OpenServerBrowser(
            '{!! url("filemanager") !!}',
            screen.width * 0.7,
            screen.height * 0.7);
}

function OpenServerBrowser(url, width, height)
{
    var iLeft = (screen.width - width) / 2;
    var iTop = (screen.height - height) / 2;
    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
    sOptions += ",width=" + width;
    sOptions += ",height=" + height;
    sOptions += ",left=" + iLeft;
    sOptions += ",top=" + iTop;
    localStorage.setItem('setUrlObj', urlobj);
    var oWindow = window.open(url, "BrowseWindow", sOptions);
}

function SetUrl(url)
{
    localStorage.setItem('setUrlObj', '');
    document.getElementById(urlobj).value = url;
    oWindow = null;
}
