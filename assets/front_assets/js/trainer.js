function validateDynamicElement(){
    $.validate({
       modules: 'location, date, security, file',
       onModulesLoaded: function () {
       }
    });
}
/***** Javascript for Trainers starts ****/
$(function () {
    $('.datetimepicker').datetimepicker({
        maskInput: true, // disables the text input mask
        pickDate: false, // disables the date picker
        pickTime: true, // disables de time picker
        pick12HourFormat: true, // enables the 12-hour format time picker
        pickSeconds: false, // disables seconds in the time picker
        //startDate: -Infinity,      // set a minimum date
        //endDate: Infinity          // set a maximum date
    });
});
var timing = $('#trainerTiming').val();
if (timing != "") {
        if (timing == "custom") {
            $('#default_div').hide('slow');
            $('#default_div').find('input').attr('disabled','disabled');
            $('#custom_div').show('slow');
            $('#custom_div').find('input').removeAttr('disabled');
        }
        if (timing == "default") {
            $('#default_div').show('slow');
            $('#default_div').find('input').removeAttr('disabled');
            $('#custom_div').hide('slow');
            $('#custom_div').find('input').attr('disabled','disabled');
        }
    }
$('#trainerTiming').change(function () {
    timing = $(this).val();
    console.log(timing);
    if (timing != "") {
        if (timing == "custom") {
            $('#default_div').hide('slow');
            $('#default_div').find('input').attr('disabled','disabled');
            $('#custom_div').show('slow');
            $('#custom_div').find('input').removeAttr('disabled');
        }
        if (timing == "default") {
            $('#default_div').show('slow');
            $('#default_div').find('input').removeAttr('disabled');
            $('#custom_div').hide('slow');
            $('#custom_div').find('input').attr('disabled','disabled');
        }
    }
})
$('#create_trainer').click(function () {
    $(this).closest('form').submit();

})
/***** Javascript for Trainers ends ****/
    
/*
$('#copyAllWeekday').click(function () {
    morningFrom = $(this).closest('#monday').find('.morningFrom').val();
    morningTo = $(this).closest('#monday').find('.morningTo').val();
    eveningFrom = $(this).closest('#monday').find('.eveningFrom').val();
    eveningTo = $(this).closest('#monday').find('.eveningTo').val();
    $('#timingSchedule').find('.morningFrom').val(morningFrom);
    $('#timingSchedule').find('.morningTo').val(morningTo);
    $('#timingSchedule').find('.eveningFrom').val(eveningFrom);
    $('#timingSchedule').find('.eveningTo').val(eveningTo);
})
$('#copyAllWeekend').click(function () {
    morningFrom = $(this).closest('#saturday').find('.morningFrom').val();
    morningTo = $(this).closest('#saturday').find('.morningTo').val();
    eveningFrom = $(this).closest('#saturday').find('.eveningFrom').val();
    eveningTo = $(this).closest('#saturday').find('.eveningTo').val();
    $('#timingSchedule').find('.weekend').filter('.morningFrom').val(morningFrom);
    $('#timingSchedule').find('.weekend').filter('.morningTo').val(morningTo);
    $('#timingSchedule').find('.weekend').filter('.eveningFrom').val(eveningFrom);
    $('#timingSchedule').find('.weekend').filter('.eveningTo').val(eveningTo);
})*/
$('body').delegate('.member_plan', 'change', function (){
    if($(this).val()=='Other'){
        row = parseInt($(this).attr('data-row'));
        $(this).parent('div').parent('div').removeClass('col-md-6 col-md-2').addClass('col-md-2');
        other_plan_div = '<div class="other_plan_name"><div class="col-md-6 other_plan_name"><div class="mem_frm_ip"><div class="txt_lbl">Package Name</div>'+
                        '<div class="ip_fld_m"><input value="" type="text" name="package['+row+'][other_name]" class="form-control" placeholder="Package Name"></div>'+
                        '</div></div>';
        other_plan_div += '<div class="col-md-6 other_plan_name"><div class="mem_frm_ip"><div class="txt_lbl">Duration</div>' +
                        '<div class="ip_fld_m"><input type="text" name="package[' + row + '][duration]" class="form-control" placeholder="30"></div>' +
                        '</div></div>';
//        other_plan_div += '<div class="col-md-4"><div class="mem_frm_ip"><div class="txt_lbl">Validity</div>' +
//                        '<div class="ip_fld_m"><input type="text" name="package[' + row + '][validity]" class="form-control" placeholder="30"></div>' +
//                        '</div></div></div>';              
         $(this).parent('div').parent('div').after(other_plan_div);
    }
    else{
        $(this).parent('div').parent('div').next('.other_plan_name').remove();
        $(this).parent('div').parent('div').removeClass('col-md-2 col-md-6').addClass('col-md-6');
    }
})

$('body').delegate('.addMorePhotoBtn', 'click', function () {
    photoRow = parseInt($('#photoRow').val());
    photoRow = photoRow + 1;
    $('#photoRow').val(photoRow);
    str = '<div class="row planDiv">' +
            '<div class="col-md-9"><div class="form-group"><label>Add Photo</label>'+
            '<input type="text" name="photos['+photoRow+']" readonly id="photo['+photoRow+']"  class="form-control">'+
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group"><label>&nbsp;</label>'+
            '<button type="button" class="btn btn-info" onclick=BrowseServer("photo['+photoRow+']");><i class="fa fa-camera"></i> Pick Image</button>'+
            '</div></div>' +
            '<div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control addMorePhotoBtn" value="Add More" id="">' +
            '</div></div><div class="col-md-1"><div class="form-group"><label>&nbsp;</label><input type="button" class="form-control deletePlanBtn" value="Delete" id=""></div></div></div>';
    $('#addMorePhoto').append(str);
})

$('body').delegate('.addMorePlanBtn', 'click', function () {
    pacakgeRow = parseInt($('#pacakgeRow').val());
    pacakgeRow = pacakgeRow + 1;
    $('#pacakgeRow').val(pacakgeRow);
    str = '<div class="row planDiv">' +
            '<div class="col-md-6"><div class="mem_frm_ip"><div class="txt_lbl">Package Name</div>' +
            '<div class="ip_fld_m"><select class="select member_plan" name="package[' + pacakgeRow + '][packageName]" data-row="' + pacakgeRow + '"><option value="">Select Package</option>'+
            '<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option>'+
            '<option value="Half yearly">Half yearly</option><option value="Annually">Annually</option>'+
            '<option value="Other">Other</option></select></div>' +
            '</div></div>'+
            '<div class="col-md-2"><div class="mem_frm_ip"><div class="txt_lbl">MRP Price</div>' +
            '<div class="ip_fld_m"><input type="text" name="package[' + pacakgeRow + '][packagePrice]" class="form-control" placeholder="2000"></div>' +
            '</div></div><div class="col-md-2"><div class="mem_frm_ip"><div class="txt_lbl">Discount</div>' +
            '<div class="ip_fld_m"><input type="text" name="package[' + pacakgeRow + '][packageDisPrice]" class="form-control" placeholder=""></div>' +
            '</div></div>' +
            '<div class="col-md-1"><div class="mem_frm_ip"><label>&nbsp;</label><div class="ip_fld_m"><input type="button" class="form-control addMorePlanBtn" value="Add More" id=""></div>' +
            '</div></div><div class="col-md-1"><div class="mem_frm_ip"><label>&nbsp;</label><div class="ip_fld_m"><input type="button" class="form-control deletePlanBtn" value="Delete" id=""></div></div></div></div>';
    $('#addMorePlan').append(str);
})

$('body').delegate('.deletePlanBtn', 'click', function () {
    $(this).closest('.planDiv').remove();
});

$('body').delegate('.addMobileBtn', 'click', function () {
    mobileRow = parseInt($('#mobileRow').val());
    mobileRow = mobileRow + 1;
    $('#mobileRow').val(mobileRow);
    str = '<div class="planDiv">' +
            ' <div class="ip_fld_m mrbt_10 add_defaut_num">'+
            ' <div class="default_num_phn">+91</div>'+
            ' <input type="hidden" name="mobile[mobile_code][]" value="+91"  />'+
            ' <input '+ 
			' data-validation-optional="true"'+
			' data-validation="number length"'+
			' data-validation-length="10-11"'+
			' data-validation-error-msg-number="Enter valid number"'+
			' data-validation-error-msg-length="Your mobile number must be between 11 digits"'+
			' name="mobile[number][]" class="form-control valid" value="" placeholder="Phone" type="text"><button class="mob_delete">-</button>'+
            '</div></div>';
            
    $('.addMobileDiv').append(str);
    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () { 
        },
        scrollToTopOnError:false,
        onSuccess : function(form) { 
            if(form.find('.subscribe_btn').length){ 
                ul = form.find('ul');
                 form.find(".already").html('');       
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN' : 'YzsmVf4ewVOKHfGQKHuOs9H6pp48um7B0K74VDqI' }, 
                });
                fromdata = form.serialize();
                 form.find(".loader").show();
                $.ajax({
                    url:"http://p3developments.com/healthhunt/user-subscribe",
                    type:'post',
                    data: fromdata,
                    success:function(data){ 
                       if(data == 1){
                             form.find(".subcribeDiv").hide();                 
                             form.find(".thankyou").show();                                            
                       }else if(data == 'already'){
                             form.find(".already").html('Email already subscribed');                                            
                       }else{
                            form.find(".already").html('Some Error Occured');       
                       }
                        form.find(".loader").hide();
                       return false;
                    },
                    error:function(err){
                         form.find(".already").html('Some Error Occured').show();
                         form.find(".loader").hide();
                        return false;
                    }
                }); 
                //return false;
            }
            else if(form.find('.ebook_subs_btn').length){ 
                var form = form;
                ul = form.find('ul');
                ul.find(".already").html('');       
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN' : 'YzsmVf4ewVOKHfGQKHuOs9H6pp48um7B0K74VDqI' }, 
                });
                fromdata = form.serialize();
                ul.find(".ebook_subs_btn").attr('disabled', 'disabled').addClass('disablebtn');
                ul.find(".ebbokloader").show();
                $.ajax({
                    url:"http://p3developments.com/healthhunt/ebook-subscribe",
                    type:'post',
                    data: fromdata,
                    success:function(data){ 
                       if(data == 1){
                           download_link = ul.find("input[name='ebook']").val();
                           //window.location.href = SITE_URL+'/'+download_link;
                           window.open(
                              SITE_URL+'/'+download_link,
                              '_blank' // <- This is what makes it open in a new window.
                            );
                            $("#ebook_download_link").attr('href', SITE_URL+'/'+download_link);              
                            $.fancybox({
                                maxWidth	: 950,
                                fitToView	: false,
                                width		: '100%',
                                autoSize	:true,
                                closeClick	: false,
                                openEffect	: 'none',
                                closeEffect	: 'none',
                                'href' : '#ebok_poop'
                            });
                            form.find("input[type='text']").val('');
                                   
                       }else if(data == 'already'){
                            ul.find(".already").html('Email already subscribed');                                            
                       }else{
                           ul.find(".already").html('Some Error Occured');       
                       }
                       ul.find(".ebbokloader").hide();
                       ul.find(".ebook_subs_btn").removeAttr('disabled').removeClass('disablebtn');
                       return false;
                    },
                    error:function(err){
                        ul.find(".already").html('Some Error Occured').show();
                        ul.find(".ebbokloader").hide();
                        ul.find(".ebook_subs_btn").removeAttr('disabled').removeClass('disablebtn');
                        return false;
                    }
                }); 
                return false;
            }
            else if(form.find('.ebook_download_pdf').length){  
                var form = form;
                ul = form.find('ul');
                ul.find(".already").html('');       
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN' : 'YzsmVf4ewVOKHfGQKHuOs9H6pp48um7B0K74VDqI' }, 
                });
                fromdata = form.serialize();
                ul.find(".ebook_download_pdf").attr('disabled', 'disabled').addClass('disablebtn');
                ul.find(".ebbokloader").show();
                $.ajax({
                    url:"http://p3developments.com/healthhunt/ebook-subscribe",
                    type:'post',
                    data: fromdata,
                    success:function(data){ 
                       if(data == 1){
                           download_link = ul.find("input[name='ebook']").val();
                           //window.location.href = SITE_URL+'/'+download_link;
                           window.open(
                              download_link,
                              '_blank' // <- This is what makes it open in a new window.
                            );
                            $("#ebook_download_link").attr('href',download_link);              
                            $.fancybox({
                                maxWidth	: 950,
                                fitToView	: false,
                                width		: '100%',
                                autoSize	:true,
                                closeClick	: false,
                                openEffect	: 'none',
                                closeEffect	: 'none',
                                'href' : '#ebok_poop'
                            });
                            form.find("input[type='text']").val('');
                                   
                       }else if(data == 'already'){
                            ul.find(".already").html('Email already subscribed');                                            
                       }else{
                           ul.find(".already").html('Some Error Occured');       
                       }
                       ul.find(".ebbokloader").hide();
                       ul.find(".ebook_download_pdf").removeAttr('disabled').removeClass('disablebtn');
                       return false;
                    },
                    error:function(err){
                        ul.find(".already").html('Some Error Occured').show();
                        ul.find(".ebbokloader").hide();
                        ul.find(".ebook_download_pdf").removeAttr('disabled').removeClass('disablebtn');
                        return false;
                    }
                }); 
                return false;
            }
            else
                return true;
            return false;
        },onError : function(form) { 
            form.find("input:text.error").first().focus();
        return false;
        
        },onElementValidate: function(form) {
            if($('.already').length && $('.already').text() !== ''){
                $('.already').text('');
            }
        }
    })
    validateDynamicElement();
})

$('body').delegate('.addLandlineBtn', 'click', function () {
    landlineRow = parseInt($('#landlineRow').val());
    landlineRow = landlineRow + 1;
    $('#landlineRow').val(landlineRow);
    str = '<div class="planDiv">' +
            '<div class="ip_fld_m mrbt_10 add_defau_land">'+
            '<div class="default_num_phn">+91</div>'+
            '<div class="default_num_land">'+
            '<input type="hidden" name="landline[landline_code][]" value="+91"  />'+
            '<input type="text" '+
            'data-validation-optional="true"'+
            'data-validation="number length"'+
            'data-validation-length="2-5"'+
            'data-validation-error-msg-length="Extension must be between 2-5 digits"'+
            'data-validation-error-msg-number="Enter valid extension"'+
            'name="landline[landline_ext][]" value="" placeholder="STD Code" />'+
            '</div>'+
            ' <input '+ 
			' data-validation-optional="true"'+
			' data-validation="number length"'+
			' data-validation-length="5-9"'+
			' data-validation-error-msg-number="Enter valid number"'+
			' data-validation-error-msg-length="Your Landline number must be between 5-9 digits"'+
			' name="landline[number][]" class="form-control valid ip_lan_num" value="" placeholder="Landline" type="text"><button class="lan_delete">-</button>'+
            '</div></div>';
            
    $('.addLandlineDiv').append(str);
    validateDynamicElement();
})

$('body').delegate('.mob_delete,.lan_delete','click', function(){
    $(this).closest('.planDiv').remove();
})

$('.associated_gym').change(function(){
   val =  $(this).val();
   if(val == 'Yes'){
       $('.associated_gym_name').removeAttr('disabled')
   }else{
       $('.associated_gym_name').attr('disabled','disabled')
       $('.associated_gym_name').val('');
   }
})
$('.closed_status').change(function(){
    if($(this).prop("checked") == true){
        $(this).closest('.timeDiv').find('input[type="text"]').attr('disabled', 'disabled');
    }else{
        $(this).closest('.timeDiv').find('input').removeAttr('disabled');
    }
});
$('.closed_status').trigger('change');

$('.time_pik').on('keydown',function(){
    return false;
})
