<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/search/{street?}/{city?}/{state?}','HomeController@Search');
Auth::routes();
Route::get('/', 'HomeController@home');
Route::get('/artist/profile', 'ProfileController@profile');
Route::get('/tattoo-lover/followers', 'ProfileController@Followers');
Route::get('/tattoo-lover/following', 'ProfileController@Following');
Route::get('/tattoo-lover/profile', 'ProfileController@profile');
Route::get('/artist/about', 'ArtistController@about');
Route::get('/studio/profile', 'ProfileController@profile');
Route::get('/studio/about', 'StudioController@about');
Route::get('/studio/followers', 'ProfileController@Followers');
Route::get('/studio/portfolio', 'ProfileController@Portfolio')->middleware('tattolover');
Route::get('/artist/portfolio', 'ProfileController@Portfolio')->middleware('tattolover');
Route::get('/studio/following', 'ProfileController@Following');
Route::get('/studio/reviews', 'ProfileController@reviews');
Route::get('/artist/followers', 'ProfileController@Followers');
Route::get('/artist/following', 'ProfileController@Following');
Route::get('/artist/reviews', 'ProfileController@Reviews');
Route::get('/admin', 'HomeController@index');
Route::get('/about-us', 'HomeController@about');
Route::post('/change-password', 'ProfileController@ChangePassword');
Route::post('/artist/addartist', 'ArtistController@SaveArtist');
Route::post('/studio/addstudio', 'StudioController@SaveStudio');
Route::post('/update-info', 'ProfileController@UpdateInfo');
Route::post('/update-logo', 'ProfileController@UpdateLogo');
Route::post('/update-cover', 'ProfileController@UpdateCover');
Route::post('/portfolio-image', 'ProfileController@AddProtfolio');
Route::post('/delete-portfolio', 'ProfileController@DeletePortfolio');


