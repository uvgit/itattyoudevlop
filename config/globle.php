<?php
return [
    'artist' => array('profile','about','reviews','followers','following','studio','portfolio'),
    'studio' => array('profile','about','reviews','followers','following','artist','portfolio'),
    'tattoo-lover' => array('profile','followers','following')
];

    
?>