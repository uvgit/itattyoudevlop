<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Session;
use Illuminate\Support\Facades\Input;
use App\User;
trait AuthenticatesUsers

{
    use RedirectsUsers, ThrottlesLogins;
    //protected $redirectTo = '/admin';
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        $email = strtolower($request->email);
        $password = $request->password;
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
	    return $this->sendLockoutResponse($request);
        }
        if(isset($request->ajax) && $request->ajax == 1) {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                if (isset(Auth::user()->role_type)) {
                    $role = Auth::user()->role_type;
                    if($role == 'artist'){
                        return 'artist';
                    }else if($role == 'studio'){
                        return 'studio';
                    }else if($role == 'tattoo_lover'){
                        return 'tattoo_lover';
                    }else{
                        return 'admin';
                    }
                }
                
            }else {
                return "Email and/or password invalid.";
            }
            
        }else {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $loggedUser = User::where('id', Auth::user()->id)->update(['online_status' => 1]);
                Session::flash('success', 'Welcome back ' . ucwords(Auth::user()->name) . '.');
                if (isset(Auth::user()->role_type)) {
                    $role = Auth::user()->role_type;
                }
                return redirect()->intended($this->redirectPath());
            }else {
                Session::flash('error', 'Email and/or password invalid.');
                return redirect()->back()->withInput(Input::except('password'));
            }
        }    
}

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $loggedUser = User::where('id', Auth::user()->id)->update(['online_status' => 0]);
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();
        
        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
